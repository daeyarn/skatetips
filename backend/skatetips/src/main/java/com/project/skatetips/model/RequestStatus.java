package com.project.skatetips.model;

public enum RequestStatus {
    PENDING,
    FULLFILLED,
    REFUSED
}
