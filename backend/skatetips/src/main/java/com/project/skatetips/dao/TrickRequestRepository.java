package com.project.skatetips.dao;

import com.project.skatetips.model.TrickRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

//repository for accesing the trick_request table
public interface TrickRequestRepository extends JpaRepository<TrickRequest, Long> {

}
