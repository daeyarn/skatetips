package com.project.skatetips.model;

public enum Stance {
    GOOFY,
    REGULAR
}
