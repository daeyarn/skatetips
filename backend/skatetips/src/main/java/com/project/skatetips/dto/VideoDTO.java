package com.project.skatetips.dto;

import com.project.skatetips.model.Trick;
import com.project.skatetips.model.VideoType;
import org.springframework.core.io.FileSystemResource;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.util.Collection;

//dto which carries information about a user's video
public class VideoDTO {

    private Long id;
    private String description;

    private Collection<Trick> tricks;
    private String video;
    private VideoType videoType;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Trick> getTricks() {
        return tricks;
    }

    public void setTricks(Collection<Trick> tricks) {
        this.tricks = tricks;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }

}
