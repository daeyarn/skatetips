package com.project.skatetips.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class Trick {
    @Id
    @GeneratedValue
    private Long id;

    private String description;

    private String name;
    private TrickStance stance;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrickStance getStance() {
        return stance;
    }

    public void setStance(TrickStance stance) {
        this.stance = stance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public static Trick createTrick(String description, String name, TrickStance stance){
        Trick trick = new Trick();
        trick.setDescription(description);
        trick.setName(name);
        trick.setStance(stance);
        return trick;
    }
}
