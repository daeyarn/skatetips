package com.project.skatetips.exceptions;

public class SkaterAlreadyExistsException extends RuntimeException{
    public SkaterAlreadyExistsException(String message) {
        super(message);
    }
}
