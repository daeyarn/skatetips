package com.project.skatetips.rest;

import com.project.skatetips.dao.SkaterRepository;
import com.project.skatetips.model.Skater;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

@Service
public class SkaterDetailsService implements UserDetailsService {

    @Autowired
    SkaterRepository skaterRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Skater skater = skaterRepository
                .findByUsername(username);
        if(skater == null) throw new UsernameNotFoundException("User " + username + " has not been found.");

        return new SkaterDetails(username, skater.getPassword(), authorities(skater));
    }

    private List<GrantedAuthority> authorities(Skater skater){
        return commaSeparatedStringToAuthorityList("ROLE_"+skater.getRole().toString());

    }
}
