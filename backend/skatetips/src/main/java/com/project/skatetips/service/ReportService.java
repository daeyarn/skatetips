package com.project.skatetips.service;

import com.project.skatetips.model.Report;

import java.util.Collection;

public interface ReportService {
    public Collection<Report> getAllReports();
    public void deleteReport(Long reportId);
    public Collection<Report> getUserReported(Long skaterId);
}
