package com.project.skatetips.rest;

import com.project.skatetips.dao.SkaterRepository;
import com.project.skatetips.dto.RegistrationDTO;
import com.project.skatetips.exceptions.SkaterAlreadyExistsException;
import com.project.skatetips.service.SkaterService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


//controller for registration
@RestController
@RequestMapping("/register")
public class RegistrationController {

    @Autowired
    SkaterService skaterService;

    @PostMapping
    public ResponseEntity<String> register(
            @RequestBody @NotNull RegistrationDTO registrationDTO
    ){
        try{
            skaterService.register(registrationDTO);
        }catch(SkaterAlreadyExistsException se){
            return new ResponseEntity<>(se.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Successfully registered.", HttpStatus.OK);
    }
}
