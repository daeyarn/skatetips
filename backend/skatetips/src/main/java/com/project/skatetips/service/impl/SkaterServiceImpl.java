package com.project.skatetips.service.impl;

import com.project.skatetips.dao.*;
import com.project.skatetips.dto.*;
import com.project.skatetips.dto.minimal.FollowingsDTO;
import com.project.skatetips.dto.minimal.RequestMinimalDTO;
import com.project.skatetips.dto.minimal.SkaterInfoMinimalDTO;
import com.project.skatetips.exceptions.*;
import com.project.skatetips.model.*;
import com.project.skatetips.service.SkaterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.FileSystemResource;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class SkaterServiceImpl implements SkaterService {

    @Autowired
    SkaterRepository skaterRepository;

    @Autowired
    RequestRepository requestRepository;

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    VideoRepository videoRepository;

    @Autowired
    TrickRepository trickRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Override
    public void register(RegistrationDTO registrationDTO) {
        Skater s = new Skater();
        if(skaterRepository.findByUsername(registrationDTO.getUsername())!=null)
            throw new SkaterAlreadyExistsException("This username is already in use.");
        s.setUsername(registrationDTO.getUsername());
        s.setDescription(registrationDTO.getDescription());
        s.setStance(registrationDTO.getStance());
        s.setRole(Role.USER);
        s.setKnownTricks(registrationDTO.getKnownTricks()
                        .stream()
                        .map(x-> {
                            if(!trickRepository.existsById(x)){
                                throw new TrickException("Trick does not exist.");
                            }
                            return trickRepository.findById(x);

                        })
                        .map(x-> x.get())
                        .collect(Collectors.toList()));
        s.setLearningTricks(registrationDTO.getLearningTricks()
                .stream()
                .map(x-> {
                    if(!trickRepository.existsById(x)){
                        throw new TrickException("Trick does not exist.");
                    }
                    return trickRepository.findById(x);

                })
                .map(x-> x.get())
                .collect(Collectors.toList()));
        s.setPassword(passwordEncoder.encode(registrationDTO.getPassword()));
        s.setFollowing(Collections.emptyList());
        s.setVideos(Collections.emptyList());

        s.setWantsHelp(registrationDTO.isWantsHelp());
        Random rand = new Random();
        int r = rand.nextInt(256);
        int g = rand.nextInt(256);
        int b = rand.nextInt(256);

        s.setProfilePictureLocation("rgb("+r+","
                                        +g+","+b+")");
        skaterRepository.saveAndFlush(s);
    }

    @Override
    public Collection<SkaterInfoMinimalDTO> getAll() {
        Collection<Skater> skaters = skaterRepository.findAll();

        return SkaterInfoMinimalDTO.getSkaterInfoDTOMinimals(skaters);
    }
    public Collection<Video> getTutorials(Long skaterId){
        Skater skater = skaterRepository.findById(skaterId).orElseThrow();

        return skater.getVideos().stream()
                .filter(v-> v.getVideoType()==VideoType.TUTORIAL)
                .collect(Collectors.toList());
    }
    public Collection<Video> getTrials(Long skaterId){
        Skater skater = skaterRepository.findById(skaterId).orElseThrow();
        Collection<VideoDTO> videoDTOs = new ArrayList<>();

        return skater.getVideos().stream()
                .filter(v-> v.getVideoType()==VideoType.TRIAL)
                .collect(Collectors.toList());

    }

    public Collection<Video> getSteezy(Long skaterId){
        Skater skater = skaterRepository.findById(skaterId).orElseThrow();
        Collection<VideoDTO> videoDTOs = new ArrayList<>();


        return skater.getVideos().stream()
                .filter(v-> v.getVideoType()==VideoType.STEEZY)
                .collect(Collectors.toList());

    }

    public VideoDTO getVideoDTO(Video v){
        VideoDTO videoDTO = new VideoDTO();

        System.out.println("/videos/"+v.getFilesystemLocation());

        videoDTO.setDescription(v.getDescription());
        videoDTO.setId(v.getId());
        videoDTO.setTricks(v.getTricks());
        videoDTO.setVideoType(v.getVideoType());

        videoDTO.setVideo(v.getFilesystemLocation());


        return videoDTO;
    }

    @Override
    public SkaterInfoDTO getSkaterInfo(Long skaterId) {

        try {
            Skater skater = skaterRepository.findById(skaterId).orElseThrow();
            Long followers = skaterRepository.getNumberOfFollowers(skaterId);
            SkaterInfoDTO sid = new SkaterInfoDTO();
            sid.setStance(skater.getStance());
            sid.setDescription(skater.getDescription());
            sid.setKnown(skater.getKnownTricks());
            sid.setKudos(null);
            sid.setLearning(skater.getLearningTricks());
            sid.setNumberOfFollowers(followers);
            sid.setUsername(skater.getUsername());
            sid.setWantsHelp(skater.isWantsHelp());
            sid.setProfilePicture(skater.getProfilePictureLocation());

            return sid;
        }catch(NoSuchElementException ne){
            throw new SkaterDoesNotExistException("This skater does not exist (anymore).");
        }
    }

    @Override
    public void followUser(Long skaterId, Long followerId) {
        try {
            Skater skater = skaterRepository.findById(skaterId).orElseThrow();
            Skater follower = skaterRepository.findById(followerId).orElseThrow();
            if (follower.getFollowing().contains(skater))
                throw new FollowerException("You are already following this user.");
            follower.getFollowing().add(skater);
            skaterRepository.saveAndFlush(follower);
        }catch(NoSuchElementException ne){
            throw new SkaterDoesNotExistException("This skater does not exist (anymore).");
        }catch(FollowerException fe){
            throw fe;
        }
    }

    @Override
    public void unfollowUser(Long skaterId, Long followerId) {
        try {
            Skater skater = skaterRepository.findById(skaterId).orElseThrow();
            Skater follower = skaterRepository.findById(followerId).orElseThrow();
            if (!follower.getFollowing().contains(skater))
                throw new FollowerException("You are not following this user.");
            follower.getFollowing().remove(skater);
            skaterRepository.saveAndFlush(follower);
        }catch(NoSuchElementException ne){
            throw new SkaterDoesNotExistException("This skater does not exist (anymore).");
        }catch(FollowerException fe){
            throw fe;
        }
    }

    @Override
    public void sendRequest(RequestDTO requestDTO) {
        try{
            Skater s = skaterRepository.findById(requestDTO.getReceiverId()).orElseThrow();
            if(requestDTO.getRequestType() == RequestType.OFFER){
                if(!s.isWantsHelp())
                    throw new SkaterAssistanceException("This user does not want assistance.");
            }
            skaterRepository.findById(requestDTO.getSenderId()).orElseThrow();

        }catch(NoSuchElementException ne){
            throw new SkaterDoesNotExistException("Skater does not exist.");
        }
        /*
        if(requestRepository.requestExists(requestDTO.getSenderId(),
                requestDTO.getReceiverId(), requestDTO.getRequestType(),
                requestDTO.getDescription()) != 0)
            throw new SkaterAssistanceException("You've already made this request.");

         */

        Request req = new Request();
        req.setDescription(requestDTO.getDescription());
        req.setSenderId(requestDTO.getSenderId());
        req.setReceiverId(requestDTO.getReceiverId());
        //req.setSatisfied(false);
        req.setType(requestDTO.getRequestType());
        req.setStatus(RequestStatus.PENDING);
        requestRepository.saveAndFlush(req);
    }

    @Override
    public void replyToRequest(RequestMinimalDTO requestMinimalDTO) {
        try{
            Request req = requestRepository.findById(requestMinimalDTO.getRequestId()).orElseThrow();
            req.setStatus(requestMinimalDTO.getStatus());
            requestRepository.saveAndFlush(req);
        }catch(NoSuchElementException ne){
            throw new RequestException("This request is no longer available.");
        }catch(RequestException re){
            throw re;
        }
    }

    @Override
    public void deleteRequest(RequestMinimalDTO requestMinimalDTO) {

        Request req = requestRepository.findById(requestMinimalDTO.getRequestId()).orElseThrow();
        if(req.getSenderId() != requestMinimalDTO.getSenderId())
            throw new RequestException("You are not the creator of this request.");
        requestRepository.delete(req);


    }

    @Override
    public Collection<Request> getRequests(Long skaterId) {
        try {
            skaterRepository.findById(skaterId).orElseThrow();
        }catch(NoSuchElementException ne){
            throw new SkaterDoesNotExistException("You do not exist.");
        }
        return requestRepository.getMyRequests(skaterId);
    }

    @Override
    public void reportUser(Report report) {
        Report rprt = new Report();
        rprt.setDescription(report.getDescription());
        rprt.setReportedId(report.getReportedId());
        rprt.setReporterId(report.getReporterId());
        reportRepository.saveAndFlush(rprt);
    }


    @Override
    public void likeVideo(Long videoId, Long likerId) {

        if(!videoRepository.existsById(videoId))
            throw new VideoException("Video does not exist.");

        if(!skaterRepository.existsById(likerId))
            throw new SkaterDoesNotExistException("You do not exist.");

        /*
        if(!likedVideoRepository.findByIds(videoId, likerId).isEmpty()){
            throw new VideoException("You've already liked this video.");
        }

        LikedVideo lv = new LikedVideo();
        lv.setVideoId(videoId);
        lv.setSkaterId(likerId);


        likedVideoRepository.saveAndFlush(lv);

         */

    }

    @Override
    public void unlikeVideo(Long videoId, Long unlikerId) {
        if(!videoRepository.existsById(videoId))
            throw new VideoException("This video does not exist.");
        if(!skaterRepository.existsById(unlikerId))
            throw new SkaterDoesNotExistException("You do not exist.");
        /*
        Optional<LikedVideo> lv = likedVideoRepository.findByIds(videoId, unlikerId);
        if(lv.isEmpty())
            throw new VideoException("You cannot unlike a video you didn't like.");

        likedVideoRepository.delete(lv.get());

         */
    }

    @Override
    public SkaterInfoMinimalDTO getRandomSkater(InteractionDTO interactionDTO) {
        ArrayList<Long> learnerIds;
        if(interactionDTO.getRequestType() == RequestType.OFFER)
            learnerIds = skaterRepository.getLearnersOfTrickInNeed(interactionDTO.getTrickId());
        else learnerIds = skaterRepository.getKnowersOfTrick(interactionDTO.getTrickId());
        learnerIds.remove(interactionDTO.getSearcherId());
        if(learnerIds.size() == 0) return null;

        Collections.shuffle(learnerIds);

        Long randomLearnerId = learnerIds.get(0);
        try{
            Skater s = skaterRepository.findById(randomLearnerId).orElseThrow();
            SkaterInfoMinimalDTO skaterInfoMinimalDTO = new SkaterInfoMinimalDTO();
            skaterInfoMinimalDTO.setDescription(s.getDescription());
            skaterInfoMinimalDTO.setId(s.getId());
            skaterInfoMinimalDTO.setUsername(s.getUsername());
            skaterInfoMinimalDTO.setProfilePicture(s.getProfilePictureLocation());
            return skaterInfoMinimalDTO;

        }catch(NoSuchElementException ne){
            throw new SkaterDoesNotExistException("Skater does not exist.");
        }
    }

    @Override
    public Collection<FollowingsDTO> getFollowings(Long skaterId) {
        ArrayList<FollowingsDTO> followings = new ArrayList<>();
        for(Long id: skaterRepository.getFollowers(skaterId)){
            Optional<Skater> skater = skaterRepository.findById(id);
            if(skater.isEmpty()) continue;
            String name = skater.get().getUsername();
            followings.add(FollowingsDTO.makeFollowingsDTO(id, name, "follower"));
        }

        for(Long id: skaterRepository.getFollowingUsers(skaterId)){
            Optional<Skater> skater = skaterRepository.findById(id);
            if(skater.isEmpty()) continue;
            String name = skater.get().getUsername();
            followings.add(FollowingsDTO.makeFollowingsDTO(id, name, "following"));
        }

        return followings;
    }

    @Override
    public Boolean isFollowing(Long follower, Long followee) {
        if(skaterRepository.isFollowing(follower, followee) != 0) return true;
        return false;
    }

    @Override
    public String getNameById(Long skaterId) {
        Skater skater;
        try{
            skater = skaterRepository.findById(skaterId).orElseThrow();
        }catch(NoSuchElementException e){
            throw new SkaterDoesNotExistException("Skater does not exist.");
        }

        return skater.getUsername();
    }

    @Override
    public void postVideo(Long skaterId, Video video) {
        Skater skater = skaterRepository.findById(skaterId).orElseThrow();
        if(skater.getVideos().size() > 30) try {
            throw new Exception("Too many tricks.");
        } catch (Exception e) {
            e.printStackTrace();
        }
        Video newVideo = new Video();
        newVideo.setDescription(video.getDescription());
        newVideo.setFilesystemLocation(video.getFilesystemLocation());
        for(Trick t: video.getTricks()){
            trickRepository.findById(t.getId()).orElseThrow();
        }
        newVideo.setTricks(video.getTricks());
        newVideo.setVideoType(video.getVideoType());

        videoRepository.saveAndFlush(newVideo);
        skater.getVideos().add(newVideo);
        skaterRepository.saveAndFlush(skater);
    }

    @Override
    public void deleteVideo(Video video, Long skaterId) {
        Video v = videoRepository.findById(video.getId()).orElseThrow();
        Skater s = skaterRepository.findById(skaterId).orElseThrow();

        s.getVideos().removeIf(vid -> vid.getId() == video.getId());

        skaterRepository.saveAndFlush(s);
        videoRepository.delete(v);

        //videoRepository.deleteLikedVideo(video.getId());
    }

    @Override
    public int updateInfo(Long skaterId, UpdateInfoDTO updateInfoDTO){
        Skater skater;
        try {
            skater = skaterRepository.findById(skaterId).orElseThrow();
            Skater skater1 = skaterRepository.findByUsername(updateInfoDTO.getUsername());
            if(skater1 != null && skater1.getId() != skaterId) return -1;
        }catch(Exception ex) {
            return -1;
        }
        skater.setUsername(updateInfoDTO.getUsername());
        skater.setDescription(updateInfoDTO.getDescription());
        skater.setProfilePictureLocation(updateInfoDTO.getProfilePictureLocation());
        skater.setLearningTricks(updateInfoDTO.getLearningTricks()
                .stream()
                .map(x-> {
                    if(!trickRepository.existsById(x)){
                        throw new TrickException("Trick does not exist.");
                    }
                    return trickRepository.findById(x);

                })
                .map(x-> x.get())
                .collect(Collectors.toList()));
        skater.setKnownTricks(updateInfoDTO.getKnownTricks()
                .stream()
                .map(x-> {
                    if(!trickRepository.existsById(x)){
                        throw new TrickException("Trick does not exist.");
                    }
                    return trickRepository.findById(x);

                })
                .map(x-> x.get())
                .collect(Collectors.toList()));

        skater.setStance(updateInfoDTO.getStance());
        skater.setWantsHelp(updateInfoDTO.getWantsHelp());
        skaterRepository.saveAndFlush(skater);
        return 0;
    }

    @Override
    public int updatePassword(Long skaterId, PasswordUpdateInfoDTO passwordUpdateInfoDTO) {
        Skater skater;
        try {
            skater = skaterRepository.findById(skaterId).orElseThrow();
        }catch(Exception e){
            return -1;
        }
        skater.setPassword(passwordEncoder.encode(passwordUpdateInfoDTO.getPassword()));
        skaterRepository.saveAndFlush(skater);

        return 0;

    }

    @Override
    public int deleteUser(Long skaterId) {
        Skater skater;
        try{
            skater = skaterRepository.findById(skaterId).orElseThrow();
            //manually deleting from each table that pertains to the skater
            //videoRepository.deleteLikedVideosBySkaterId(skaterId); //table liked_video
            reportRepository.deleteBySkaterId(skaterId); //table report
            requestRepository.deleteBySkaterId(skaterId); //table request
            //from the following tables rows where the skater_id == skaterId will
            //automatically be deleted when skater is deleted
            //table skater_following
            skaterRepository.deleteFromSkaterFollowing(skaterId);
            //table skater_known_tricks
            skaterRepository.deleteFromSkaterKnown(skaterId);
            //table skater_learning_tricks
            skaterRepository.deleteFromSkaterLearning(skaterId);

            //table skater_videos
            skaterRepository.deleteFromSkaterVideos(skaterId);

            for(Video v: skater.getVideos()) videoRepository.delete(v);
            skaterRepository.delete(skater);
        }catch(Exception ex){
            return -1;
        }

        return 0;
    }
}
