package com.project.skatetips.dto.minimal;

//dto for transfering followers and followings
public class FollowingsDTO {
    private Long userId;
    private String userName;
    private String followType;

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getFollowType() {
        return followType;
    }

    public void setFollowType(String followType) {
        this.followType = followType;
    }

    public static FollowingsDTO makeFollowingsDTO(Long id, String name, String followType){
        FollowingsDTO followingsDTO = new FollowingsDTO();
        followingsDTO.setFollowType(followType);
        followingsDTO.setUserId(id);
        followingsDTO.setUserName(name);
        return followingsDTO;
    }
}
