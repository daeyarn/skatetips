package com.project.skatetips.rest;

import com.project.skatetips.dto.InteractionDTO;
import com.project.skatetips.dto.minimal.SkaterInfoMinimalDTO;
import com.project.skatetips.exceptions.SkaterDoesNotExistException;
import com.project.skatetips.service.SkaterService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

//controller which handles the "/interact" endpoint
@RestController
@RequestMapping("/interact")
public class InteractionController {

    @Autowired
    SkaterService skaterService;

    @PutMapping
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<SkaterInfoMinimalDTO> getRandomSkater(
            @RequestBody @NotNull InteractionDTO interactionDTO
            ){
        try{
            SkaterInfoMinimalDTO s = skaterService.getRandomSkater(interactionDTO);
            return new ResponseEntity<>(s, HttpStatus.OK);
        }catch(SkaterDoesNotExistException se){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

}
