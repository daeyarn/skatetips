package com.project.skatetips.dao;

import com.project.skatetips.model.Request;
import com.project.skatetips.model.RequestType;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

//repository for accessing the request table
public interface RequestRepository extends JpaRepository<Request, Long> {

    @Query("Select r from Request r where r.senderId = ?1 or r.receiverId = ?1")
    public Collection<Request> getMyRequests(Long skaterId);

    @Query("Select count(r) from Request r where " +
            "r.senderId = ?1 and r.receiverId = ?2 and r.type = ?3 and " +
            "r.description = ?4")
    public Long requestExists(Long senderId, Long receiverId, RequestType requestType, String description);

    @Modifying
    @Transactional
    @Query(value = "delete from request where receiver_id = ?1 or sender_id = ?1", nativeQuery = true)
    public void deleteBySkaterId(Long skaterId);
}
