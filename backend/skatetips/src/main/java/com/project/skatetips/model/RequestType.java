package com.project.skatetips.model;

public enum RequestType {
    OFFER,
    QUESTION
}
