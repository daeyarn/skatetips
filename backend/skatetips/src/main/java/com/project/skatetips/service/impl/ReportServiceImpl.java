package com.project.skatetips.service.impl;

import com.project.skatetips.dao.ReportRepository;
import com.project.skatetips.dao.SkaterRepository;
import com.project.skatetips.exceptions.SkaterDoesNotExistException;
import com.project.skatetips.model.Report;
import com.project.skatetips.model.Skater;
import com.project.skatetips.service.ReportService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.NoSuchElementException;

//service which handles operations for reporting users
@Service
public class ReportServiceImpl implements ReportService {

    @Autowired
    ReportRepository reportRepository;

    @Autowired
    SkaterRepository skaterRepository;

    @Override
    public Collection<Report> getAllReports() {
        return reportRepository.findAll();
    }

    @Override
    public void deleteReport(Long reportId) {
        try{
            reportRepository.deleteById(reportId);
        }catch(Exception e){
            throw new NoSuchElementException("There is no report with that id.");
        }
    }

    @Override
    public Collection<Report> getUserReported(Long skaterId) {
        Collection<Report> reports;
        try{
            Skater skater = skaterRepository.findById(skaterId).orElseThrow();
        }catch(NoSuchElementException ex){
            throw new SkaterDoesNotExistException("Skater does not exist.");
        }

        reports = reportRepository.getUserReported(skaterId);
        return reports;
    }
}
