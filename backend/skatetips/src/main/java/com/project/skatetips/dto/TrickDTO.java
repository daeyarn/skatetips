package com.project.skatetips.dto;

import com.project.skatetips.model.TrickStance;

public class TrickDTO {

    private String description;
    private String name;
    private TrickStance stance;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public TrickStance getStance() {
        return stance;
    }

    public void setStance(TrickStance stance) {
        this.stance = stance;
    }
}
