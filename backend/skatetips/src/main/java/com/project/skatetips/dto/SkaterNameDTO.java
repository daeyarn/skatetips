package com.project.skatetips.dto;

//dto for returning a skater's name
public class SkaterNameDTO {
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
