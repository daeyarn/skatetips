package com.project.skatetips.rest;

import com.project.skatetips.exceptions.SkaterDoesNotExistException;
import com.project.skatetips.model.Report;
import com.project.skatetips.service.ReportService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.Collections;

//controller which handles certain actions pertaining to reports
@RestController
@RequestMapping("/reports")
public class ReportController {

    @Autowired
    ReportService reportService;

    @GetMapping
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Collection<Report>> getAllReports(){
        return new ResponseEntity<>(reportService.getAllReports(), HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/{skaterId}")
    @Secured("ROLE_USER")
    public ResponseEntity<Collection<Report>> getUserReported(
            @PathVariable Long skaterId
    ){
        try{
            Collection<Report> reports = reportService.getUserReported(skaterId);
            return new ResponseEntity<>(reports, HttpStatus.OK);
        }catch(SkaterDoesNotExistException ex){
            return new ResponseEntity<>(Collections.emptyList(), HttpStatus.BAD_REQUEST);
        }
    }
    @DeleteMapping
    @RequestMapping("/delete/{reportId}")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> deleteReport(
            @PathVariable @NotNull Long reportId
    ){
        try{
            reportService.deleteReport(reportId);
            return new ResponseEntity<>("Successfully deleted the report.", HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }
}
