package com.project.skatetips.model;

import javax.persistence.*;
import java.util.Collection;

@Entity
public class Video {

    @Id
    @GeneratedValue
    private Long id;
    private String description;
    @ManyToMany
    private Collection<Trick> tricks;
    private String filesystemLocation;
    private VideoType videoType;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Trick> getTricks() {
        return tricks;
    }

    public void setTricks(Collection<Trick> tricks) {
        this.tricks = tricks;
    }

    public String getFilesystemLocation() {
        return filesystemLocation;
    }

    public void setFilesystemLocation(String filesystemLocation) {
        this.filesystemLocation = filesystemLocation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public VideoType getVideoType() {
        return videoType;
    }

    public void setVideoType(VideoType videoType) {
        this.videoType = videoType;
    }
}
