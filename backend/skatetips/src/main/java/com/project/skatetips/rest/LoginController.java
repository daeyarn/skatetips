package com.project.skatetips.rest;

import com.project.skatetips.dto.LoginDTO;
import com.project.skatetips.dto.SkaterInfoDTO;
import com.project.skatetips.service.SkaterService;
import com.sun.istack.NotNull;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

//simulating login during development, not actually used
@RestController
@RequestMapping("/login")
public class LoginController {

    SkaterService skaterService;

    @PostMapping
    public ResponseEntity<SkaterInfoDTO> login(
        @RequestBody @NotNull LoginDTO loginDTO
    ){
        return null;


    }
}
