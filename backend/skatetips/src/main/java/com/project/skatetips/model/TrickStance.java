package com.project.skatetips.model;

public enum TrickStance {
    NATURAL,
    SWITCH,
    NOLLIE,
    FAKIE
}
