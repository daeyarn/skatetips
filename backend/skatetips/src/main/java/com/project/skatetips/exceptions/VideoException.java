package com.project.skatetips.exceptions;

public class VideoException extends RuntimeException{
    public VideoException(String message) {
        super(message);
    }
}
