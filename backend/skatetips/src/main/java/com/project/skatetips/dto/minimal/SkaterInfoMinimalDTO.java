package com.project.skatetips.dto.minimal;

import com.project.skatetips.model.Skater;

import java.util.Collection;
import java.util.LinkedList;

//minimal skater info that is sent to front-end during interaction
public class SkaterInfoMinimalDTO {
    private Long id;
    private String username;
    private String description;
    private String profilePicture;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public static Collection<SkaterInfoMinimalDTO> getSkaterInfoDTOMinimals(Collection<Skater> skaters){
        Collection<SkaterInfoMinimalDTO> skaterInfoMinimalDTOS = new LinkedList<>();
        for(Skater s: skaters){
            SkaterInfoMinimalDTO skaterInfoMinimalDTO = new SkaterInfoMinimalDTO();
            skaterInfoMinimalDTO.setId(s.getId());
            skaterInfoMinimalDTO.setDescription(s.getDescription());
            skaterInfoMinimalDTO.setUsername(s.getUsername());
            skaterInfoMinimalDTO.setProfilePicture(s.getProfilePictureLocation());
            skaterInfoMinimalDTOS.add(skaterInfoMinimalDTO);
        }

        return skaterInfoMinimalDTOS;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
