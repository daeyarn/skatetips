package com.project.skatetips.dto.minimal;

public class FilesystemLocationDTO {
    String filesystemLocation;

    public String getFilesystemLocation() {
        return filesystemLocation;
    }

    public void setFilesystemLocation(String filesystemLocation) {
        this.filesystemLocation = filesystemLocation;
    }
}
