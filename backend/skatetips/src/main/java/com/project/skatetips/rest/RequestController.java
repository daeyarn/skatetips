package com.project.skatetips.rest;

import com.project.skatetips.dto.RequestDTO;
import com.project.skatetips.dto.minimal.RequestMinimalDTO;
import com.project.skatetips.dto.minimal.SkaterIdDTO;
import com.project.skatetips.exceptions.RequestException;
import com.project.skatetips.exceptions.SkaterAssistanceException;
import com.project.skatetips.exceptions.SkaterDoesNotExistException;
import com.project.skatetips.model.Request;
import com.project.skatetips.service.SkaterService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;
import java.util.NoSuchElementException;

//controller handling functions at the requests endpoint
@RestController
@RequestMapping("/requests")
public class RequestController {

    @Autowired
    SkaterService skaterService;

    @PostMapping
    @RequestMapping("/send")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<String> sendRequestForHelp(
            @RequestBody @NotNull RequestDTO requestDTO
    ){
        if(requestDTO.getReceiverId() == requestDTO.getSenderId())
            return new ResponseEntity<>("You cannot send a request to yourself.", HttpStatus.BAD_REQUEST);
        try{
            skaterService.sendRequest(requestDTO);
            return new ResponseEntity("Request successfully sent.", HttpStatus.OK);
        }catch(SkaterDoesNotExistException sde){
            return new ResponseEntity<>(sde.getMessage(), HttpStatus.BAD_REQUEST);
        }catch(SkaterAssistanceException sa){
            return new ResponseEntity<>(sa.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping
    @RequestMapping("/my-requests")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<Collection<Request>> getMyRequests(
            @RequestBody @NotNull SkaterIdDTO skaterDTO
    ){
        try{
            return new ResponseEntity(skaterService.getRequests(skaterDTO.getSkaterId()), HttpStatus.OK);
        }catch(SkaterDoesNotExistException se){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    @RequestMapping("/my-requests/delete")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<String> deleteMyRequestForHelp(
            @RequestBody @NotNull RequestMinimalDTO requestMinimalDTO
    ) {
        try {
            skaterService.deleteRequest(requestMinimalDTO);
            return new ResponseEntity("Request successfully deleted.", HttpStatus.OK);
        }catch(RequestException ex){
            return new ResponseEntity<>(ex.getLocalizedMessage(), HttpStatus.BAD_REQUEST);
        }catch(NoSuchElementException ne){
            return new ResponseEntity<>(ne.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PutMapping
    @RequestMapping("/my-requests/reply")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<String> replyToRequestForHelp(
            @RequestBody RequestMinimalDTO requestMinimalDTO
    ){
        try {
            skaterService.replyToRequest(requestMinimalDTO);
            return new ResponseEntity("Request successfully answered.", HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

}
