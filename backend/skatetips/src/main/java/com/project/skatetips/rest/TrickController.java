package com.project.skatetips.rest;

import com.project.skatetips.dao.TrickRepository;
import com.project.skatetips.dto.TrickDTO;
import com.project.skatetips.exceptions.TrickException;
import com.project.skatetips.model.Trick;
import com.project.skatetips.model.TrickRequest;
import com.project.skatetips.service.TrickService;
import com.sun.istack.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;

import java.util.Collection;

//controller for actions regarding tricks
@RestController
@RequestMapping("/tricks")
public class TrickController {

    @Autowired
    TrickService trickService;

    @GetMapping
    public Collection<Trick> getAllTricks() { return trickService.getAllTricks(); }

    @PostMapping
    @RequestMapping("/add")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> addTrick(
            @RequestBody @NotNull TrickDTO trickDTO
    ) {
        try{
            trickService.addNewTrick(trickDTO);
            return new ResponseEntity<>("Trick successfully added.", HttpStatus.OK);
        }catch(TrickException te){
            return new ResponseEntity<>(te.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @PostMapping
    @RequestMapping("/suggest")
    @Secured("ROLE_USER")
    public ResponseEntity<String> suggestTrick(
            @RequestBody @NotNull TrickDTO tr
    ){
        trickService.addTrickSuggestion(tr);
        return new ResponseEntity<>("Successfully requested trick.", HttpStatus.OK);
    }
    @PutMapping
    @RequestMapping("/update")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> editTrick(
           @RequestBody @NotNull Trick trick
    ){
        try{
            trickService.updateTrick(trick);
            return new ResponseEntity<>("Trick updated.", HttpStatus.OK);
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

    }

    @GetMapping
    @RequestMapping("/suggestions")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<Collection<TrickRequest>> getAllSuggestions(){

        Collection<TrickRequest> suggestions = trickService.getAllSuggestions();
        return new ResponseEntity<>(suggestions, HttpStatus.OK);
    }

    @DeleteMapping
    @RequestMapping("/suggestions/{suggestionId}/delete")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> deleteSuggestion(
            @PathVariable Long suggestionId
    ){
        try {
            trickService.deleteSuggestion(suggestionId);
            return new ResponseEntity<>("Succesfully deleted the suggestion.", HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>("This suggestion does not exist.", HttpStatus.BAD_REQUEST);
        }
    }

    @DeleteMapping
    @RequestMapping("/{trickId}/delete")
    @Secured("ROLE_ADMIN")
    public ResponseEntity<String> deleteTrick(
            @PathVariable Long trickId
    ){
        try {
            trickService.deleteTrick(trickId);
            return new ResponseEntity<>("Succesfully deleted the trick.", HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>("This trick does not exist.", HttpStatus.BAD_REQUEST);
        }
    }

}
