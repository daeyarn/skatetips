package com.project.skatetips.service;

import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

@Service
public interface VideoService {

    public String addVideo(MultipartFile video);

}
