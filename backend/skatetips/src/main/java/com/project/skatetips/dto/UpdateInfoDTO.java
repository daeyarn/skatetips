package com.project.skatetips.dto;

import com.project.skatetips.model.Stance;

import java.util.Collection;

//dto used for updating some information visible on profile
public class UpdateInfoDTO {
    private String username;
    private Stance stance;
    private String description;
    private Boolean wantsHelp;
    private Collection<Long> knownTricks;
    private Collection<Long> learningTricks;
    private String profilePictureLocation;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Stance getStance() {
        return stance;
    }

    public void setStance(Stance stance) {
        this.stance = stance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Boolean getWantsHelp() {
        return wantsHelp;
    }

    public void setWantsHelp(Boolean wantsHelp) {
        this.wantsHelp = wantsHelp;
    }

    public Collection<Long> getKnownTricks() {
        return knownTricks;
    }

    public void setKnownTricks(Collection<Long> knownTricks) {
        this.knownTricks = knownTricks;
    }

    public Collection<Long> getLearningTricks() {
        return learningTricks;
    }

    public void setLearningTricks(Collection<Long> learningTricks) {
        this.learningTricks = learningTricks;
    }

    public String getProfilePictureLocation() {
        return profilePictureLocation;
    }

    public void setProfilePictureLocation(String profilePictureLocation) {
        this.profilePictureLocation = profilePictureLocation;
    }
}
