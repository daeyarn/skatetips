package com.project.skatetips.exceptions;

public class SkaterDoesNotExistException extends RuntimeException {
    public SkaterDoesNotExistException(String message) {
        super(message);
    }
}
