package com.project.skatetips.service;

import com.project.skatetips.dto.TrickDTO;
import com.project.skatetips.model.Trick;
import com.project.skatetips.model.TrickRequest;

import java.util.Collection;

public interface TrickService {
    public Collection<Trick> getAllTricks();
    public void addNewTrick(TrickDTO trickDTO);
    public void updateTrick(Trick trick);
    public void addTrickSuggestion(TrickDTO tr);
    public Collection<TrickRequest> getAllSuggestions();
    public void deleteSuggestion(Long suggestionId);
    public void deleteTrick(Long trickId);
}
