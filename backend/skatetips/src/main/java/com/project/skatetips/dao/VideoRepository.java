package com.project.skatetips.dao;

import com.project.skatetips.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

//repository for accessing the video table
public interface VideoRepository extends JpaRepository<Video, Long> {

    /*
    @Query(value = "delete from liked_video where video_id = ?1", nativeQuery = true)
    public void deleteLikedVideo(Long id);

    @Transactional
    @Modifying
    @Query(value = "delete from liked_video where skater_id = ?1", nativeQuery = true)
    public void deleteLikedVideosBySkaterId(Long skaterId);

     */
}
