package com.project.skatetips.model;



import javax.persistence.*;
import java.util.Collection;
import java.util.Objects;
import java.util.Set;


@Entity
public class Skater {

    @Id
    @GeneratedValue
    private Long id;

    //@UniqueConstraint()
    private String username;

    private Stance stance;

    private String description;

    private Role role;

    private String profilePictureLocation;

    @ManyToMany
    private Collection<Trick> knownTricks;
    @ManyToMany
    private Collection<Trick> learningTricks;

    private boolean wantsHelp;

    private String password;

    @OneToMany
    private Collection<Video> videos;

    @ManyToMany
    private Collection<Skater> following;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Stance getStance() {
        return stance;
    }

    public void setStance(Stance stance) {
        this.stance = stance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Trick> getKnownTricks() {
        return knownTricks;
    }

    public void setKnownTricks(Collection<Trick> knownTricks) {
        this.knownTricks = knownTricks;
    }

    public Collection<Trick> getLearningTricks() {
        return learningTricks;
    }

    public void setLearningTricks(Collection<Trick> wishlistTricks) {
        this.learningTricks = wishlistTricks;
    }

    public boolean isWantsHelp() {
        return wantsHelp;
    }

    public void setWantsHelp(boolean wantsHelp) {
        this.wantsHelp = wantsHelp;
    }



    public Collection<Skater> getFollowing() {
        return following;
    }

    public void setFollowing(Collection<Skater> following) {
        this.following = following;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Skater)) return false;
        Skater skater = (Skater) o;
        return id.equals(skater.id) &&
                username.equals(skater.username);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, username);
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Video> getVideos() {
        return videos;
    }

    public void setVideos(Collection<Video> videos) {
        this.videos = videos;
    }

    public String getProfilePictureLocation() {
        return profilePictureLocation;
    }

    public void setProfilePictureLocation(String profilePictureLocation) {
        this.profilePictureLocation = profilePictureLocation;
    }
}
