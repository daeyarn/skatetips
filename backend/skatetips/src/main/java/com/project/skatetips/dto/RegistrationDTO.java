package com.project.skatetips.dto;

import com.project.skatetips.model.Role;
import com.project.skatetips.model.Stance;
import com.project.skatetips.model.Trick;

import java.util.Collection;

//data which is needed to register a user
public class RegistrationDTO {

    private String username;

    private Stance stance;

    private String description;

    private Collection<Long> knownTricks;

    private Collection<Long> learningTricks;

    private boolean wantsHelp;

    private String password;

    private String profilePicture;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Stance getStance() {
        return stance;
    }

    public void setStance(Stance stance) {
        this.stance = stance;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    public boolean isWantsHelp() {
        return wantsHelp;
    }

    public void setWantsHelp(boolean wantsHelp) {
        this.wantsHelp = wantsHelp;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Collection<Long> getKnownTricks() {
        return knownTricks;
    }

    public void setKnownTricks(Collection<Long> knownTricks) {
        this.knownTricks = knownTricks;
    }

    public Collection<Long> getLearningTricks() {
        return learningTricks;
    }

    public void setLearningTricks(Collection<Long> learningTricks) {
        this.learningTricks = learningTricks;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
