package com.project.skatetips.dao;

import com.project.skatetips.model.Report;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

//repository for accessing the report table
public interface ReportRepository extends JpaRepository<Report, Long> {

    @Query(value = "select * from report where reporter_id = ?1", nativeQuery = true)
    public Collection<Report> getUserReported(Long skaterId);

    @Modifying
    @Transactional
    @Query(value = "delete from report where reported_id = ?1 or reporter_id = ?1", nativeQuery = true)
    public void deleteBySkaterId(Long skaterId);
}
