package com.project.skatetips.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.skatetips.dao.SkaterRepository;
import com.project.skatetips.model.Skater;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Date;
import java.util.NoSuchElementException;
import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import  static com.project.skatetips.rest.SecurityConstants.*;
import static org.springframework.security.core.authority.AuthorityUtils.commaSeparatedStringToAuthorityList;

//authentication filter, issues a new jwt, serves as login endpoint
public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    private AuthenticationManager authenticationManager;

    private SkaterRepository skaterRepository;


    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, SkaterRepository skaterRepository) {
        this.authenticationManager = authenticationManager;
        this.skaterRepository = skaterRepository;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req,
                                                HttpServletResponse res) throws AuthenticationException {
        try {
            Skater creds = new ObjectMapper()
                    .readValue(req.getInputStream(), Skater.class);

            Skater u = skaterRepository
                    .findByUsername(creds
                            .getUsername());
            if(u == null) throw new NoSuchElementException("That username is not present.");

            return authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            creds.getUsername(),
                            creds.getPassword(),
                            commaSeparatedStringToAuthorityList(
                                    skaterRepository
                                            .findByUsername(creds
                                                    .getUsername())
                                            .toString()
                            )

                    )
            );
        } catch (Exception e) {
            if(e instanceof IOException)
                throw new RuntimeException(e);
            else if(e instanceof NoSuchElementException) //throw new NoSuchElementException("User not existent.");
                res.setStatus(404);
            else if(e instanceof  AuthenticationException) //throw new NoSuchElementException("Failed to login.");
                res.setStatus(401);
        }

        return null;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req,
                                            HttpServletResponse res,
                                            FilterChain chain,
                                            Authentication auth) throws IOException, ServletException {

        Skater authUser = skaterRepository.findByUsername(((SkaterDetails) auth.getPrincipal()).getUsername());
        String token = JWT.create()
                .withSubject(((SkaterDetails) auth.getPrincipal()).getUsername())
                .withExpiresAt(new Date(System.currentTimeMillis() + EXPIRATION_TIME))
                .withClaim("rol", auth.getAuthorities().toString())
                .withClaim("id",
                        skaterRepository.findByUsername(
                                ((SkaterDetails)auth.getPrincipal()).getUsername()).getId()
                )
                .withClaim("usrname", authUser.getUsername())
                .withClaim("role", authUser.getRole().toString())
                .sign(Algorithm.HMAC512(SECRET.getBytes()));
        res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
    }

}