package com.project.skatetips.rest;

import com.project.skatetips.dao.SkaterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.access.channel.ChannelProcessingFilter;
import static com.project.skatetips.rest.SecurityConstants.*;

//class for web security
@EnableWebSecurity
@EnableGlobalMethodSecurity(securedEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {



    @Autowired
    SkaterRepository skaterRepository;

    @Autowired
    SkaterDetailsService skaterDetailsService;
    BCryptPasswordEncoder bCryptPasswordEncoder;
    public WebSecurity(BCryptPasswordEncoder bCryptPasswordEncoder) {
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
            http.cors().and().csrf().disable()
                    .addFilter(new JWTAuthenticationFilter(authenticationManager(), skaterRepository))
                    .addFilter(new JWTAuthorizationFilter(authenticationManager(), skaterRepository))
                    .addFilterBefore(new WebSecurityCorsFilter(), ChannelProcessingFilter.class)
                    .authorizeRequests()
                    .antMatchers(HttpMethod.POST, SIGN_UP_URL, LOGIN_URL).permitAll()
                    .antMatchers(HttpMethod.PUT, SIGN_UP_URL, LOGIN_URL).permitAll()
                    .antMatchers(HttpMethod.GET, TRICKS_URL).permitAll()
                    .anyRequest().authenticated()
                    .and()
                    // this disables session creation on Spring Security
                    .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        }



    @Override
    public void configure(AuthenticationManagerBuilder auth) throws Exception {
        auth.userDetailsService(skaterDetailsService).passwordEncoder(bCryptPasswordEncoder);
    }
}
