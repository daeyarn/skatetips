package com.project.skatetips.dto;

import com.project.skatetips.model.RequestType;
import com.project.skatetips.model.Trick;


//data that back-end needs to return a random skater
public class InteractionDTO {
    private Long searcherId;
    private RequestType requestType;
    private Long trickId;

    public RequestType getRequestType() {
        return requestType;
    }

    public void setRequestType(RequestType requestType) {
        this.requestType = requestType;
    }

    public Long getTrickId() {
        return trickId;
    }

    public void setTrickId(Long trickId) {
        this.trickId = trickId;
    }

    public Long getSearcherId() {
        return searcherId;
    }

    public void setSearcherId(Long searcherId) {
        this.searcherId = searcherId;
    }
}
