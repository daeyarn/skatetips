package com.project.skatetips.service.impl;

import com.project.skatetips.dao.SkaterRepository;
import com.project.skatetips.dao.TrickRepository;
import com.project.skatetips.model.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.*;
import javax.annotation.PostConstruct;

import static java.lang.Math.abs;

//service for filling up the database with users for simulation
@Service
public class DataLoadingService {

    @Autowired
    TrickRepository trickRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    SkaterRepository skaterRepository;

    Trick trick = new Trick();
    Skater skater;
    @PostConstruct
    public void loadTricks(){
        trick.setStance(TrickStance.NATURAL);
        trick.setName("Kickflip");
        trick.setDescription("Rotation of the board along the longer axis.");

        Trick trick1 = Trick.createTrick("Simple jump. The base for many other tricks.",
                "Ollie",
                TrickStance.NATURAL);
        Trick trick2= Trick.createTrick("Simple jump. The base for many other tricks.",
                "Ollie",
                TrickStance.FAKIE);
        Trick trick3 = Trick.createTrick("Simple jump. The base for many other tricks.",
                "Ollie",
                TrickStance.SWITCH);
        Trick trick4 = Trick.createTrick("Simple jump. The base for many other tricks.",
                "Ollie",
                TrickStance.NOLLIE);
        Trick trick5 = Trick.createTrick("Frontside 180 rotation of the board.",
                "Frontside shuvit",
                TrickStance.NATURAL);

        trickRepository.saveAndFlush(trick);
        trickRepository.saveAndFlush(trick1);
        trickRepository.saveAndFlush(trick2);
        trickRepository.saveAndFlush(trick3);
        trickRepository.saveAndFlush(trick4);
        trickRepository.saveAndFlush(trick5);

    }

    @PostConstruct
    public void loadSkaters(){
        String[] usernames = {
                "Christina", "Chloe", "Kylie", "Karli", "Aisha", "Nia", "Gabriella", "Kaylie",
                "Kendall", "Mila", "Raina", "Felicity", "Austin",
                "Alberto",
                "Dario",
                "Aldo",
                "Marvin",
                "Brogan",
                "Salvador",
                "Quintin",
                "Luca",
                "Derek",
                "Nicolas",
                "Ivan", "dejan"
        };

        Random rnd = new Random();
        ArrayList<Trick> allTricks = (ArrayList)trickRepository.findAll();


        for(int i = 0; i< usernames.length; i++){
            skater = new Skater();
            //int number = rnd.nextInt(usernames.length);
            int number = i;
            String skaterName = usernames[number];
            Stance stance = Stance.REGULAR;
            if(rnd.nextBoolean()) stance = Stance.GOOFY;
            String description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, " +
                    "sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim " +
                    "ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex " +
                    "ea commodo consequat.";
            description = description.substring(0, rnd.nextInt(description.length()));
            ArrayList<Trick> knownTricks = new ArrayList<>();

            Collections.shuffle(allTricks);
            for(int j = 0; j<abs(rnd.nextInt(abs(allTricks.size()))); j++){
                knownTricks.add(allTricks.get(j));
            }

            ArrayList<Trick> learningTricks = new ArrayList<>();
            Collections.shuffle(allTricks);
            for(int j = 0; j<abs(rnd.nextInt(abs(allTricks.size()))); j++){
                learningTricks.add(allTricks.get(j));
            }

            Role role = Role.ADMIN;
            if(rnd.nextInt(10) >= 8) role = Role.USER;
            Boolean wantsHelp = rnd.nextBoolean();
            String password = Long.toString(System.currentTimeMillis());
            if(skaterName == "dejan") password = "dejan";
            skater.setDescription(description);
            skater.setWantsHelp(wantsHelp);
            skater.setKnownTricks(knownTricks);
            skater.setLearningTricks(learningTricks);

            skater.setFollowing(Collections.emptyList());
            skater.setUsername(skaterName);
            skater.setStance(stance);
            skater.setRole(role);
            skater.setPassword(passwordEncoder.encode(password));
            skater.setVideos(Collections.emptyList());
            Random rand = new Random();
            int r = rand.nextInt(256);
            int g = rand.nextInt(256);
            int b = rand.nextInt(256);

            skater.setProfilePictureLocation("rgb("+r+","
                    +g+","+b+")");

            skaterRepository.saveAndFlush(skater);
        }

    }
}
