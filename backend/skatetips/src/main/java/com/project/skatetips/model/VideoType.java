package com.project.skatetips.model;

public enum VideoType {
    TUTORIAL,
    TRIAL,
    STEEZY
}
