package com.project.skatetips.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.Objects;

@Entity
public class Request {
    @Id
    @GeneratedValue
    private Long id;

    private Long senderId;
    private Long receiverId;
    private String description;

    private RequestStatus status;
    private RequestType type;


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getSenderId() {
        return senderId;
    }

    public void setSenderId(Long senderId) {
        this.senderId = senderId;
    }

    public Long getReceiverId() {
        return receiverId;
    }

    public void setReceiverId(Long receiverId) {
        this.receiverId = receiverId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RequestStatus getStatus() {
        return status;
    }

    public void setStatus(RequestStatus status) {
        this.status = status;
    }





    public RequestType getType() {
        return type;
    }

    public void setType(RequestType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Request)) return false;
        Request request = (Request) o;
        return
                senderId.equals(request.senderId) &&
                receiverId.equals(request.receiverId) &&
                description.equals(request.description) &&
                status == request.status &&
                type == request.type;
    }

    @Override
    public int hashCode() {
        return Objects.hash(senderId, receiverId, description, status, type);
    }
}
