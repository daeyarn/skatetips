package com.project.skatetips.service;

import com.project.skatetips.dto.*;
import com.project.skatetips.dto.minimal.FollowingsDTO;
import com.project.skatetips.dto.minimal.RequestMinimalDTO;
import com.project.skatetips.dto.minimal.SkaterInfoMinimalDTO;
import com.project.skatetips.model.Report;
import com.project.skatetips.model.Request;
import com.project.skatetips.model.Video;

import java.io.File;
import java.util.Collection;

public interface SkaterService {
    void register(RegistrationDTO registrationDTO);
    Collection<SkaterInfoMinimalDTO> getAll();
    Collection<Video> getTutorials(Long skaterId);
    Collection<Video> getTrials(Long skaterId);
    Collection<Video> getSteezy(Long skaterId);
    SkaterInfoDTO getSkaterInfo(Long skaterId);
    void followUser(Long skaterId, Long followerId);
    void unfollowUser(Long skaterId, Long followerId);
    void sendRequest(RequestDTO requestDTO);
    void replyToRequest(RequestMinimalDTO requestMinimalDTO);
    void deleteRequest(RequestMinimalDTO requestMinimalDTO);
    void reportUser(Report report);
    public Collection<Request> getRequests(Long skaterId);
    public void likeVideo(Long videoId, Long likerId);
    public void unlikeVideo(Long videoId, Long unlikerId);
    public SkaterInfoMinimalDTO getRandomSkater(InteractionDTO interactionDTO);
    public Collection<FollowingsDTO> getFollowings(Long skaterId);
    public Boolean isFollowing(Long follower, Long followee);
    public String getNameById(Long skaterId);
    public void postVideo(Long skaterId, Video video);
    public void deleteVideo(Video video, Long skaterId);
    public int updateInfo(Long skaterId, UpdateInfoDTO updateInfoDTO);
    public int updatePassword(Long skaterId, PasswordUpdateInfoDTO passwordUpdateInfoDTO);
    public int deleteUser(Long skaterId);

}
