package com.project.skatetips.exceptions;

public class TrickException extends RuntimeException{
    public TrickException(String message) {
        super(message);
    }
}
