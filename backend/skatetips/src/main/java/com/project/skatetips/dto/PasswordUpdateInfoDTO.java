package com.project.skatetips.dto;

//dto for updating password
public class PasswordUpdateInfoDTO {
    private String password;

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
