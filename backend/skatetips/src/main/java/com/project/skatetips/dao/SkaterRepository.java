package com.project.skatetips.dao;

import com.project.skatetips.model.Skater;
import com.project.skatetips.model.Video;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.Collection;

//repository for accessing the skater table
public interface SkaterRepository extends JpaRepository<Skater, Long> {

    @Query(value="select id from skater_learning_tricks join skater on " +
            "skater_id =id where wants_help = true and learning_tricks_id = ?1  ", nativeQuery = true)
    public ArrayList<Long> getLearnersOfTrickInNeed(Long trickId);

    @Query(value="select id from skater_known_tricks join skater on " +
            "skater_id = id where known_tricks_id = ?1  ", nativeQuery = true)
    public ArrayList<Long> getKnowersOfTrick(Long trickId);

    @Query(value="select skater_id from skater_following where following_id= ?1", nativeQuery = true)
    public ArrayList<Long> getFollowers(Long userId);

    @Query(value="select following_id from skater_following where skater_id = ?1", nativeQuery = true)
    public ArrayList<Long> getFollowingUsers(Long userId);

    @Query(value = "select count(*) from skater_following where skater_id = ?1 and following_id = ?2", nativeQuery = true)
    public Long isFollowing(Long follower, Long followee);

    @Query("Select s from Skater s where s.username = ?1")
    public Skater findByUsername(String username);

    @Query(value="select count(skater_id) from skater_following where following_id = ?1 ", nativeQuery = true)
    public Long getNumberOfFollowers(Long skaterId);

    @Modifying
    @Transactional
    @Query(value = "delete from skater_following where skater_id = ?1 or following_id = ?1", nativeQuery = true)
    void deleteFromSkaterFollowing(Long skaterId);

    @Modifying
    @Transactional
    @Query(value = "delete from skater_known_tricks where skater_id = ?1", nativeQuery = true)
    void deleteFromSkaterKnown(Long skaterId);

    @Modifying
    @Transactional
    @Query(value = "delete from skater_learning_tricks where skater_id = ?1", nativeQuery = true)
    void deleteFromSkaterLearning(Long skaterId);

    /*
    @Query(value = "delete from skater_steezy where skater_id = ?1", nativeQuery = true)
    void deleteFromSkaterSteezy(Long skaterId);

    @Query(value = "delete from skater_trials where skater_id = ?1", nativeQuery = true)
    void deleteFromSkaterTrials(Long skaterId);

    @Query(value = "delete from skater_tutorials where skater_id = ?1", nativeQuery = true)
    void deleteFromSkaterTutorials(Long skaterId);
    */

    @Modifying
    @Transactional
    @Query(value = "delete from skater_videos where skater_id = ?1", nativeQuery = true)
    void deleteFromSkaterVideos(Long skaterId);
}
