package com.project.skatetips.service.impl;

import com.project.skatetips.dao.TrickRepository;
import com.project.skatetips.dao.TrickRequestRepository;
import com.project.skatetips.dto.TrickDTO;
import com.project.skatetips.exceptions.TrickException;
import com.project.skatetips.model.Trick;
import com.project.skatetips.model.TrickRequest;
import com.project.skatetips.service.TrickService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

//trick service, service for the trick controller
@Service
public class TrickServiceImpl implements TrickService {

    @Autowired
    TrickRepository trickRepository;

    @Autowired
    TrickRequestRepository trickRequestRepository;

    @Override
    public void addNewTrick(TrickDTO trickDTO) {
        if(trickRepository.existsByDTO(trickDTO.getName(), trickDTO.getStance()) != 0)
            throw new TrickException("Trick already exists.");
        Trick trick = new Trick();
        trick.setDescription(trickDTO.getDescription());
        trick.setName(trickDTO.getName());
        trick.setStance(trickDTO.getStance());
        trickRepository.saveAndFlush(trick);
    }

    @Override
    public Collection<Trick> getAllTricks() {
        return trickRepository.findAll();
    }

    @Override
    public void updateTrick(Trick trick) {
        Optional<Trick> t = trickRepository.findById(trick.getId());
        if(t.isEmpty()) throw new TrickException("This trick does not exist.");

        t.get().setStance(trick.getStance());
        t.get().setDescription(trick.getDescription());
        t.get().setName(trick.getName());

        trickRepository.saveAndFlush(t.get());
    }

    @Override
    public void addTrickSuggestion(TrickDTO tr) {
        TrickRequest trickRequest = new TrickRequest();
        trickRequest.setDescription(tr.getDescription());
        trickRequest.setName(tr.getName());

        trickRequestRepository.saveAndFlush(trickRequest);
    }

    @Override
    public Collection<TrickRequest> getAllSuggestions() {
        return trickRequestRepository.findAll();
    }

    @Override
    public void deleteSuggestion(Long suggestionId) {
        trickRequestRepository.deleteById(suggestionId);
    }

    @Override
    public void deleteTrick(Long trickId) {
        try{
            Trick t = trickRepository.findById(trickId).orElseThrow();
            String name = t.getName();

            Collection<Trick> tricksWithThatName = trickRepository.findAllByName(name);
            for(Trick trick: tricksWithThatName) {
                trickRepository.deleteFromKnownTricks(trick.getId());
                trickRepository.deleteFromLearningTricks(trick.getId());
                trickRepository.deleteFromVideos(trick.getId());
                trickRepository.delete(trick);
            }
        }catch(Exception ex){
            throw ex;
        }
    }
}
