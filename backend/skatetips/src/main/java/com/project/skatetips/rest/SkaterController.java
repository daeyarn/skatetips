package com.project.skatetips.rest;

import com.project.skatetips.dto.*;
import com.project.skatetips.dto.minimal.*;
import com.project.skatetips.exceptions.FollowerException;
import com.project.skatetips.exceptions.SkaterDoesNotExistException;
import com.project.skatetips.exceptions.VideoException;
import com.project.skatetips.model.Report;
import com.project.skatetips.model.Skater;
import com.project.skatetips.model.Video;
import com.project.skatetips.service.SkaterService;
import com.project.skatetips.service.VideoService;
import com.sun.istack.NotNull;
import org.apache.coyote.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Required;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Collection;

//skater controller for actions regarding skaters
@RestController
@RequestMapping("/users")
public class SkaterController {

    @Autowired
    SkaterService skaterService;

    @GetMapping
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public Collection<SkaterInfoMinimalDTO> getAllSkaters(){
        return skaterService.getAll();
    }

    @GetMapping
    @RequestMapping("/{skaterId}/name")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<SkaterNameDTO> getSkaterName(
            @PathVariable Long skaterId
    ){
        try{
            String name = skaterService.getNameById(skaterId);
            SkaterNameDTO snd = new SkaterNameDTO();
            snd.setUsername(name);
            return new ResponseEntity<>(snd, HttpStatus.OK);
        }catch(SkaterDoesNotExistException ex){
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }
    @GetMapping
    @RequestMapping("/{skaterId}/info")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<SkaterInfoDTO> getSkaterInfo(
            @PathVariable @NotNull Long skaterId){
        try{
            return new ResponseEntity(skaterService.getSkaterInfo(skaterId), HttpStatus.OK);
        }catch(SkaterDoesNotExistException se) {
            return new ResponseEntity<>(null, HttpStatus.BAD_REQUEST);
        }
    }

    @GetMapping
    @RequestMapping("/{skaterId}/tutorials")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<Collection<Video>> getTutorials(
            @PathVariable @NotNull Long skaterId){
        return new ResponseEntity(skaterService.getTutorials(skaterId), HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/{skaterId}/trials")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<Collection<Video>> getTrials(
            @PathVariable @NotNull Long skaterId){
        return new ResponseEntity(skaterService.getTrials(skaterId), HttpStatus.OK);
    }

    @GetMapping
    @RequestMapping("/{skaterId}/steezy")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<Collection<Video>> getSteezy(
            @PathVariable @NotNull Long skaterId){
        return new ResponseEntity(skaterService.getSteezy(skaterId), HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("/{skaterId}/follow")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<String> followUser(
            @PathVariable @NotNull Long skaterId,
            @RequestBody SkaterIdDTO followerDTO){

        if(skaterId == followerDTO.getSkaterId())
            return new ResponseEntity<>("You cannot follow yourself.", HttpStatus.BAD_REQUEST);
        try {
            skaterService.followUser(skaterId, followerDTO.getSkaterId());
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("You are now following the user.", HttpStatus.OK);
    }

    @PutMapping
    @RequestMapping("/{skaterId}/unfollow")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<String> unfollowUser(
            @PathVariable @NotNull Long skaterId,
            @RequestBody SkaterIdDTO followerDTO){

        if(skaterId == followerDTO.getSkaterId())
            return new ResponseEntity<>("You cannot unfollow yourself.", HttpStatus.BAD_REQUEST);

        try{
            skaterService.unfollowUser(skaterId, followerDTO.getSkaterId());
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity("You are not following the user anymore.", HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("/{skaterId}/following")
    @Secured({"ROLE_USER", "ROLE_ADMIN"})
    public ResponseEntity<Boolean> isFollowing(
            @PathVariable @NotNull Long skaterId,
            @RequestBody @NotNull SkaterIdDTO skaterIdDTO
            ){
        if(skaterId == skaterIdDTO.getSkaterId()) return new ResponseEntity<>(false, HttpStatus.BAD_REQUEST);
        Boolean following = skaterService.isFollowing(skaterIdDTO.getSkaterId(), skaterId);
        return new ResponseEntity<>(following, HttpStatus.OK);

    }

    @PostMapping
    @RequestMapping("/{skaterId}/followings")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<Collection<FollowingsDTO>> getFollowings(
            @PathVariable @NotNull Long skaterId,
            @RequestBody @NotNull SkaterIdDTO skaterSenderId
    ){
        if(skaterId == skaterSenderId.getSkaterId()) return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
        Collection<FollowingsDTO> followings = skaterService.getFollowings(skaterId);
        return new ResponseEntity<>(followings, HttpStatus.OK);
    }




    @PostMapping
    @RequestMapping("/{skaterId}/report")
    @Secured("ROLE_USER")
    public ResponseEntity<String> reportUser(
            @RequestBody @NotNull Report report
    ){
        skaterService.reportUser(report);
        return new ResponseEntity("User successfully reported.", HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("/{skaterId}/videos/{videoId}/like")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> likeVideo(
            @PathVariable @NotNull Long skaterId,
            @PathVariable @NotNull Long videoId,
            @RequestBody SkaterIdDTO likerDTO
    ){
        try{
            skaterService.likeVideo(videoId, likerDTO.getSkaterId());
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Video successfully liked", HttpStatus.OK);

    }

    @DeleteMapping
    @RequestMapping("/{skaterId}/videos/{videoId}/unlike")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> unlikeVideo(
            @PathVariable @NotNull Long skaterId,
            @PathVariable @NotNull Long videoId,
            @RequestBody SkaterIdDTO unlikerDTO
    ){
        try{
            skaterService.unlikeVideo(videoId, unlikerDTO.getSkaterId());
        }catch(Exception e){
            return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
        }
        return new ResponseEntity<>("Video successfully unliked", HttpStatus.OK);

    }
    /*
    @PostMapping
    @RequestMapping("/videos/send")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<FilesystemLocationDTO> addVideoFile(

            @RequestParam("inpFile") MultipartFile video
    ){
        FilesystemLocationDTO fsl = new FilesystemLocationDTO();
        if(video == null) return new ResponseEntity<>(fsl, HttpStatus.BAD_REQUEST);

        fsl.setFilesystemLocation(null);
        try{

            String path = videoService.addVideo(video);
            fsl.setFilesystemLocation(path);
            return new ResponseEntity<>(fsl, HttpStatus.CREATED);
        }catch(Exception e){
            return new ResponseEntity<>(fsl, HttpStatus.CONFLICT);
        }

    }

    */

    @DeleteMapping
    @RequestMapping("/{skaterId}/videos/delete")
    public ResponseEntity<String> deleteVideo(
            @PathVariable Long skaterId,
            @RequestBody Video video
    ){
        try{
            skaterService.deleteVideo(video, skaterId);
            return new ResponseEntity<>("Video successfully deleted", HttpStatus.OK);
        }catch(Exception ex){
            return new ResponseEntity<>(ex.getMessage(), HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping
    @RequestMapping("/{skaterId}/info/update")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> updateInfo(
            @PathVariable Long skaterId,
            @RequestBody UpdateInfoDTO updateInfoDTO
    ){
        int status = skaterService.updateInfo(skaterId, updateInfoDTO);
        if(status == -1) return new ResponseEntity<>("That username is already in use by another user that is not you.", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>("Successfully updated info.", HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("/{skaterId}/password/update")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> updatePassword(
            @PathVariable Long skaterId,
            @RequestBody PasswordUpdateInfoDTO updateInfoDTO
    ){
        int status = skaterService.updatePassword(skaterId, updateInfoDTO);
        if(status == -1) return new ResponseEntity<>("That skater doesn't exist.", HttpStatus.BAD_REQUEST);
        return new ResponseEntity<>("Successfully updated password.", HttpStatus.OK);
    }

    @PostMapping
    @RequestMapping("/{skaterId}/videos/post")
    @Secured({"ROLE_ADMIN", "ROLE_USER"})
    public ResponseEntity<String> postVideo(
            @PathVariable Long skaterId,
            @RequestBody Video video
    ){
        try {
            skaterService.postVideo(skaterId, video);
            return new ResponseEntity<>("Everything went fine.", HttpStatus.OK);
        }catch(Exception ex){
            System.out.println(ex);
            return new ResponseEntity<>("Nothing went fine.", HttpStatus.CONFLICT);
        }
    }

    @DeleteMapping
    @RequestMapping("/{userId}/delete")
    @Secured({"ROLE_ADMIN","ROLE_USER"})
    public ResponseEntity deleteUser(
        @PathVariable Long userId
    ){
        System.out.println(userId);
        if(skaterService.deleteUser(userId) == -1) return new ResponseEntity("Something went wrong.", HttpStatus.BAD_REQUEST);;
        return new ResponseEntity("Succesfully removed.", HttpStatus.OK);
    }



}
