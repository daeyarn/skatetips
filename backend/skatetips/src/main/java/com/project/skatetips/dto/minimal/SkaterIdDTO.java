package com.project.skatetips.dto.minimal;

//skater id dto which just carries the id
public class SkaterIdDTO {
    Long skaterId;

    public Long getSkaterId() {
        return skaterId;
    }

    public void setSkaterId(Long skaterId) {
        this.skaterId = skaterId;
    }
}
