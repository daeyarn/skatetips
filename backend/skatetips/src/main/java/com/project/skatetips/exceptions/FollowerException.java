package com.project.skatetips.exceptions;

public class FollowerException extends RuntimeException{
    public FollowerException(String message) {
        super(message);
    }
}
