package com.project.skatetips.model;

public enum Role {
    USER,
    ADMIN
}
