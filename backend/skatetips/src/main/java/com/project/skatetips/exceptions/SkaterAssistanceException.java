package com.project.skatetips.exceptions;

public class SkaterAssistanceException extends RuntimeException{
    public SkaterAssistanceException(String message) {
        super(message);
    }
}
