package com.project.skatetips.dto;

import com.project.skatetips.model.Skater;
import com.project.skatetips.model.Stance;
import com.project.skatetips.model.Trick;

import java.util.Collection;

//information about skater visible on their profile
public class SkaterInfoDTO {
    private String username;
    private String description;
    private String profilePicture;
    private Collection<Trick> known;
    private Collection<Trick> learning;
    private Long kudos;
    private Stance stance;
    private Long numberOfFollowers;
    private boolean wantsHelp;

    public Stance getStance() {
        return stance;
    }

    public void setStance(Stance stance) {
        this.stance = stance;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Collection<Trick> getKnown() {
        return known;
    }

    public void setKnown(Collection<Trick> known) {
        this.known = known;
    }

    public Collection<Trick> getLearning() {
        return learning;
    }

    public void setLearning(Collection<Trick> learning) {
        this.learning = learning;
    }

    public Long getKudos() {
        return kudos;
    }

    public void setKudos(Long kudos) {
        this.kudos = kudos;
    }

    public Long getNumberOfFollowers() {
        return numberOfFollowers;
    }

    public void setNumberOfFollowers(Long numberOfFollowers) {
        this.numberOfFollowers = numberOfFollowers;
    }

    public boolean isWantsHelp() {
        return wantsHelp;
    }

    public void setWantsHelp(boolean wantsHelp) {
        this.wantsHelp = wantsHelp;
    }

    public String getProfilePicture() {
        return profilePicture;
    }

    public void setProfilePicture(String profilePicture) {
        this.profilePicture = profilePicture;
    }
}
