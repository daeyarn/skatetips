package com.project.skatetips.dao;

import com.project.skatetips.model.Trick;
import com.project.skatetips.model.TrickStance;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;

//repository for accessing the trick table
public interface TrickRepository extends JpaRepository<Trick, Long> {

    @Query("Select count(t) from Trick t where t.name = ?1 and t.stance = ?2")
    public Long existsByDTO(String name, TrickStance stance);

    @Modifying
    @Transactional
    @Query(value = "Delete from skater_known_tricks where known_tricks_id = ?1", nativeQuery = true)
    public void deleteFromKnownTricks(Long trickId);

    @Modifying
    @Transactional
    @Query(value = "Delete from skater_learning_tricks where learning_tricks_id = ?1", nativeQuery = true)
    public void deleteFromLearningTricks(Long trickId);

    @Modifying
    @Transactional
    @Query(value = "Delete from video_tricks where tricks_id = ?1", nativeQuery = true)
    public void deleteFromVideos(Long trickId);


    @Query("select trick from Trick trick where trick.name = :name")
    public Collection<Trick> findAllByName(String name);




}
