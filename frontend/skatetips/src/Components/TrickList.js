import Axios from 'axios';
import React from 'react'
import Trick from "./Trick.js"

function TrickList(props){
    const [tricks, setTricks] = React.useState([])

    React.useEffect(() => {
        fetch("tricks")
        .then(response => response.json())
        .then(tricks => setTricks(tricks))
        .catch(error => console.log(error));
    },[]);

    return (
        <div>
            {tricks.map(trick => <Trick learning = {props.learning} key = {trick+props.id} trick = {trick} />)}
        </div>
    );



}


export default TrickList;