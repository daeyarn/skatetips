import React, { useState } from 'react';
import ReactDOM from 'react';


function LoginForm(props){
    const [form, setForm] = React.useState({username:'', password:''});


    function isValid(){ //checks whether the inputs in the form can be sent
        const {username, password} = form
        return username != '' && password != '';

    }
    function onChange(e){ //updating the value from the value given in input to form.username
        const {name,value} = e.target;
        setForm(oldForm => ({...oldForm, [name]: value}))
    }

    
    function onSubmit(){
        
        let rspnse = null;
        let options = {
            method: "POST",
            headers: {
                "Content-Type": "application/json"
            }
        }
  
                        
    }
    return <div>
        <h1>LOGIN</h1>
        <form onSubmit = {onSubmit}>
            <div>
                <label>Username</label>
                <input name="username" onChange = {onChange} value = {form.username}></input>
            </div>
            <div>
                <label>Password</label>
                <input name= "password" onChange = {onChange} value = {form.password}></input>
            </div>
            <div>
                <button name="login" disabled={!isValid}></button>
            </div>
        </form>
    </div>

}

export default LoginForm;