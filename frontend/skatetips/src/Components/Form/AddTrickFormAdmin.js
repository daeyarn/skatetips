import React from 'react'
import { Field, Form, Formik } from 'formik'
import "./AddTrickFormAdmin.css"
import Trick from "../../Components/Trick.js"
import TrickSuggestionList from "../../Components/TrickSuggestionList.js"

function AddTrickFormUser(props) {

    const [tricks, setTricks] = React.useState([]);
    let selectedTrick = null;

    React.useEffect(() => {
        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("/tricks", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(tricks => {
                setTricks(tricks);
                for (let i = 0; i < tricks.length; i++) {
                    document.getElementById("single-trick-add-trick-div-" + tricks[i].id).addEventListener('click', function () {

                        if (selectedTrick == null) {

                            selectedTrick = tricks[i].name.replace(" ", "_") + tricks[i].id;
                            document.getElementById("update-trick-button").disabled = false;
                            document.getElementById(selectedTrick).style.borderColor = "blue";
                            document.getElementById("edit-trick-div-trick-name").innerHTML = "trick name: " + tricks[i].name + ", " + tricks[i].stance;
                            document.getElementById("edit-trick-div-trick-dscrption").innerHTML = "trick description: " + tricks[i].description;
                        }

                        else if (selectedTrick != null) {
                            document.getElementById(selectedTrick).style.borderColor = "white";

                            //OVDJE PROBLEM JER NE BUDE UVIJEK SAMO JEDNA ZNAMENKA ID MSM DA JE RIJESENO!!!:DD
                            if (selectedTrick == tricks[i].name.replace(" ", "_") + tricks[i].id) {
                                selectedTrick = null;
                                document.getElementById("update-trick-button").disabled = true;
                                document.getElementById("edit-trick-div-trick-name").innerHTML = "trick name: not selected";
                                document.getElementById("edit-trick-div-trick-dscrption").innerHTML = "trick description: not selected";
                                return;
                            }
                            selectedTrick = tricks[i].name.replace(" ", "_") + tricks[i].id;
                            document.getElementById(selectedTrick).style.borderColor = "blue";
                            document.getElementById("edit-trick-div-trick-name").innerHTML = "trick name: " + tricks[i].name + ", " + tricks[i].stance;
                            document.getElementById("edit-trick-div-trick-dscrption").innerHTML = "trick description: " + tricks[i].description;
                        }

                    })
                    document.getElementById("single-trick-add-trick-div-" + tricks[i].id).addEventListener("contextmenu", e => {
                        e.preventDefault();
                        /*if(selectedTrick != null) document.getElementById(selectedTrick).style.borderColor = "white";
                        document.getElementById(tricks[i].name.replace(" ", "_") + tricks[i].id).style.borderColor = "red";
                        console.log(document.getElementById(tricks[i].name.replace(" ", "_") + tricks[i].id).style.borderColor = "red")*/
                        if (window.confirm("Are you sure you want to delete "+tricks[i].name+"?")) {
                            let options = {
                                method: "DELETE",
                                headers: {
                                    "Content-type": "application/json",
                                    "Authorization": localStorage.getItem("token")
                                }
                            }

                            fetch("/tricks/"+tricks[i].id+"/delete", options)
                                .then(response => {
                                    if(response.status != 200)
                                        alert("Something went wrong. Please reload the page.")
                                        window.location.reload()
                                    
                                })
                            
                        }
                        /*
                        else{
                            document.getElementById(tricks[i].name.replace(" ", "_") + tricks[i].id).style.borderColor = "white";
                        }*/
                    })

                }


            })


    }, [])

    return (
        <div className="scrollable-add-trick-div">
            <div id="upper-half-add-trick-div">
                <div id="trick-admin-form-div">

                    <h3>Add a trick!</h3>
                    <hr />
                    <span>
                        Add your own trick, edit an existing trick's name and/or description, delete a trick or make a trick from other users' suggestions.
                        By pressing send the trick will be created for all four stances.
                    </span>
                    <br />
                    <br />
                    <Formik initialValues={{ "trick_name": '', "trick_description": '' }}
                        onSubmit={(data, actions) => {
                            if (data.trick_name == '' || data.trick_description == '') {
                                alert("Please enter trick name and description.")
                                return;
                            }
                            let lowerTrickName = data.trick_name.toLowerCase().trim().replace(" ", "_");
                            let existingTricks = document.getElementsByClassName("trick-user-add-form");


                            if (document.getElementsByClassName(lowerTrickName).length != 0) {
                                console.log(lowerTrickName)
                                let yis = alert("This trick might already exist.")

                                return;
                            }


                            let stances = ["FAKIE", "NATURAL", "SWITCH", "NOLLIE"]
                            for (let i = 0; i < 4; i++) {
                                let options = {
                                    method: "POST",
                                    headers: {
                                        "Content-type": "application/json",
                                        "Authorization": localStorage.getItem("token")
                                    },
                                    body: JSON.stringify({
                                        "description": data.trick_description,
                                        "name": data.trick_name,
                                        "stance": stances[i]
                                    })
                                }

                                fetch("/tricks/add", options)
                                    .then(response => {
                                        console.log(response)
                                        if (response.status == 200) {
                                            actions.resetForm();
                                        }
                                        if(i == 3){
                                            window.location.reload();
                                        }
                                    })
                            }

                            
                        }}>
                        <Form id="add-trick-admin-form">
                            <Field id="add-trick-admin-form-name" as="textarea" name="trick_name" placeholder="trick name"></Field>
                            <br />
                            <Field id="add-trick-admin-form-description" as="textarea" name="trick_description" placeholder="trick description"></Field>
                            <br />
                            <button type="submit" id="add-trick-admin-button">Send</button>
                        </Form>
                    </Formik>
                </div>
                <div id="existing-tricks-container">
                    <h3 id="existing-tricks-header" className="existing-tricks-header">Existing tricks(left-click to edit, right-click to delete trick)</h3>
                    <span>*editing only alters information for that (trick, stance) pair, not the trick itself, while deleting a trick deletes every (trick, stance) pair</span>
                    <br />
                    <br />
                    {tricks.length != 0 &&
                        <div id="tricks-for-admin-add-trick">

                            {tricks.map(trick =>
                                <div className="single-trick-add-trick-div" id={"single-trick-add-trick-div-" + trick.id}>
                                    <Trick trick={trick} idsAreNames={true} />
                                </div>
                            )}
                        </div>

                    }
                </div>
                <div id="edit-trick-div">
                    <h3 className="existing-tricks-header" marginBottom="0%">New information </h3>
                    <br />
                    <span>
                        *leave blank the field you don't want to change, the trick name and description will only be changed for that stance
                    </span>
                    <hr />
                    <span id="edit-trick-div-trick-name">trick name: not selected </span>
                    <br />
                    <span id="edit-trick-div-trick-dscrption">trick description: not selected</span>
                    <br />

                    <br />
                    <Formik initialValues={{ "new_name": "", "new_description": "" }}
                        onSubmit={(data, actions) => {

                            if (data.new_description == "" && data.new_name == "") return;
                            let nameAndStance = document.getElementById("edit-trick-div-trick-name").innerHTML.replace("trick name: ", "").split(",")
                            let name = nameAndStance[0].trim();
                            let stance = nameAndStance[1].trim();
                            //console.log(stance)
                            //console.log(name)
                            let description = document.getElementById("edit-trick-div-trick-dscrption").innerHTML.replace("trick description: ", "").trim()
                            if (data.new_description != "") description = data.new_description;

                            let tricksWithName = document.getElementsByClassName(name.toLowerCase().replace(" ", "_"))

                            let id = null;

                            for (let i = 0; i < tricksWithName.length; i++) {
                                if (tricksWithName[i].innerHTML.includes(stance)) {
                                    id = parseInt(tricksWithName[i].id.toLowerCase().replace(name.toLowerCase().replace(" ", "_"), ""))
                                    break
                                }
                            }

                            console.log(id)
                            let oldName = name;
                            if (id == null) return
                            if (data.new_name != "") name = data.new_name;

                            let options = {
                                method: "PUT",
                                headers: {
                                    "Content-type": "application/json",
                                    "Authorization": localStorage.getItem("token")
                                },
                                body: JSON.stringify({
                                    "id": id,
                                    "description": description,
                                    "name": name,
                                    "stance": stance
                                }),
                            }

                            fetch("/tricks/update", options)
                                .then(response => {
                                    //console.log(response)
                                    window.location.reload();

                                })


                        }}>
                        <Form>

                            <div id="fields-add-trick">

                                <Field className="add-trick-edit-field" as="textarea" name="new_name" placeholder="new name"></Field>

                                <br />
                                <Field className="add-trick-edit-field" as="textarea" name="new_description" placeholder="new description"></Field>

                            </div>

                            <button id="update-trick-button" type="submit" disabled>Edit</button>
                        </Form>
                    </Formik>
                </div>
            </div>
            <hr />
            <div id="lower-add-trick-div">
                <span>Suggestions(<span  id = "number-of-suggestions">0</span>)</span>
                <TrickSuggestionList />

            </div>
        </div>
    );

}

export default AddTrickFormUser;