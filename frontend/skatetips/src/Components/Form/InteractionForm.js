import React, { useState, useEffect } from 'react';
import TrickList from "./../TrickList.js"
import { Field, Form, Formik, FormikProps } from 'formik';
import RandomSkater from '../Interaction/RandomSkater.js';
import "./InteractionForm.css"
import Trick from "../Trick.js"
import { Tooltip } from '@varld/popover';

function InteractionForm(props) {
    
    const [randomSkater, setRandomSkater] = React.useState(null)
    const [state, setState] = React.useState(false);
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let userId = bearer.id;
   

    let offerType = null;
    let trick = null;

    const [tricks, setTricks] = React.useState([])
    React.useEffect(() => {
        localStorage.setItem("trickRequest", "");

        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("tricks", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(tricks => {
                setTricks(tricks)
                document.getElementById("trick-to-choose-div").style.height = "20%";
            })
            .catch(error => console.log(error));

        document.getElementById("offer-help-div").addEventListener('click', function () {
            if (offerType == "OFFER") {
                document.getElementById("offer-help-div").style.borderColor = "white";
                offerType = null;
                return;
            }
            document.getElementById("offer-help-div").style.borderColor = "blue";
            document.getElementById("request-help-div").style.borderColor = "white";
            offerType = "OFFER";

        })

        document.getElementById("request-help-div").addEventListener('click', function () {

            if (offerType == "QUESTION") {
                document.getElementById("request-help-div").style.borderColor = "white";
                offerType = null;
                return;
            }
            document.getElementById("request-help-div").style.borderColor = "blue";
            document.getElementById("offer-help-div").style.borderColor = "white";
            offerType = "QUESTION"
        })

        document.getElementById("interaction-form-submit-div").addEventListener('click', function () {
            if (localStorage.getItem("trickRequest") == "" || offerType == null) {
                alert("Please select offer type and trick.")
                return;
            }

            trick = parseInt(localStorage.getItem("trickRequest").replace("trick", ""))
            console.log(offerType, trick)
            var options = {
                method: 'PUT',
                headers: {
                    "Content-type": "application/json",
                    "Authorization": localStorage.getItem("token"),
                },
                body: JSON.stringify({
                    searcherId: userId,
                    requestType: offerType,
                    trickId: trick
                }),

            };

            fetch("/interact", options).then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
                .then(randomSkater => {
                    if (randomSkater == null) {
                        console.log("There are no such skaters. :c")
                        alert("There are currently no skaters that satisfy the criteria.")
                        return;
                    }
                    setRandomSkater(randomSkater)
                    
                    if(document.getElementById("random-skater-photo-div") != null)
                    document.getElementById("random-skater-photo-div").style.backgroundColor = randomSkater.profilePicture;
                    
                    /*
                    if(document.getElementById("random-skater-photo-div") != null){
                        if(randomSkater.profilePicture != null)
                        document.getElementById("random-skater-photo-div").style.backgroundImage = "url("+randomSkater.profilePicture+");"
                    }
                    console.log(randomSkater);*/
                    let thatDiv = document.getElementById("some-div");
                    thatDiv.style.width = "45vw";

                    setState(true);
                })
                .catch(error => alert("No skater in that criteria."));





        })

    }, []);

    return (

        <div className="main-div">
            <div id="some-div" className="some-div">
                <label>I want to </label>
                <br />
                <br />
                <div className="offer-type-selection-div">
                    <div id="offer-help-div-div" className="offer-help-div-div">
                        <Tooltip display="inline" content="offer help">
                            <div id="offer-help-div" className="offer-help-div">

                            </div>
                        </Tooltip>
                    </div>
                    &nbsp;&nbsp;
                    <div id="request-help-div-div" className="request-help-div-div">
                        <Tooltip display="inline" content="request help">
                            <div id="request-help-div" className="request-help-div">

                            </div>
                        </Tooltip>
                    </div>
                </div>
                <br />
                <label> with </label>
                <br />
                <br />
                <div id = "trick-to-choose-div" className="trick-to-choose-div">
                    {tricks.length != 0 && tricks.map(trick =>
                        <div id={trick.id + "trick"}>
                            <Trick trick={trick} interact={true}></Trick>
                        </div>)
                    }
                </div>
                <br />
                <div id="interaction-form-submit-div" className="interaction-form-submit-div">

                </div>
            </div>
            {state == true && randomSkater != undefined && randomSkater != null && <RandomSkater id="random-skater" skater={randomSkater} />}


        </div>



    );
}

export default InteractionForm;