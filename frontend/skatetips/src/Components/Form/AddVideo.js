import { Form, Formik, Field } from 'formik'
import React from 'react'
import "./AddVideo.css"
import Trick from "../Trick.js"
import { Tooltip } from '@varld/popover'

function AddForm(props) {
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let myId = bearer.id;

    let tricksInVideo = []


    const [tricks, setTricks] = React.useState([])
    React.useEffect(() => {
        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("/tricks", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(tricks => {
                setTricks(tricks)
                for (let i = 0; i < tricks.length; i++) {
                    if (document.getElementById("add-video-trick-" + tricks[i].id) == null) return;
                    document.getElementById("add-video-trick-" + tricks[i].id).addEventListener("click", function () {
                        console.log(tricksInVideo)
                        if (document.getElementById("single-trick-in-video-" + tricks[i].id + "trick").style.borderColor == "green") {

                            tricksInVideo.pop(tricksInVideo.indexOf(tricks[i].id))
                            document.getElementById("single-trick-in-video-" + tricks[i].id + "trick").style.borderColor = "white"
                            let numberOfTricks = parseInt(document.getElementById("number-of-tricks-in-video-form").innerHTML)
                            numberOfTricks--;
                            document.getElementById("number-of-tricks-in-video-form").innerHTML = numberOfTricks
                        }
                        else {

                            tricksInVideo.push(tricks[i].id)
                            document.getElementById("single-trick-in-video-" + tricks[i].id + "trick").style.borderColor = "green"
                            let numberOfTricks = parseInt(document.getElementById("number-of-tricks-in-video-form").innerHTML)
                            numberOfTricks++;
                            document.getElementById("number-of-tricks-in-video-form").innerHTML = numberOfTricks
                        }
                    })
                }

            })

    }, [])
    return (
        <div id="video-input-div">
            <div id="add-video-header-div">
                {/*<Tooltip content="You can have up to 30 videos in total, with each being less than 10MB in size.">*/}
                    <span id="add-video-header">Add video to <span id="add-video-header-section"></span> section!</span>
                    {/*<button id="info-about-video-adding" disabled>?</button>*/}
                {/*</Tooltip>*/}

                <hr />
            </div>

            <div id="add-video-form" >
                <Formik initialValues={{ "video": "", "video_description": "" }}
                    onSubmit={(data, actions) => {
                        document.getElementById("send-video").disabled = true;
                        if (data.video == "") {
                            alert("Make sure to add the video link before sending.")
                            document.getElementById("send-video").disabled = false;
                            return
                        }

                        //let inpFile = document.getElementById("video-choice")

                        let tricksInVid = []
                        for (let i = 0; i < tricks.length; i++) {
                            if (document.getElementById("single-trick-in-video-" + tricks[i].id + "trick").style.borderColor == "green") {
                                tricksInVid.push(tricks[i])
                            }
                        }
                        /*
                        const formData = new FormData();
                        formData.append("inpFile", inpFile.files[0]);

                        let options1 = {


                            method: "POST",
                            headers: {
                                "Authorization": localStorage.getItem("token"),
                                

                            },
                            body: formData,
                        }
                        */
                        let videoType = document.getElementById("add-video-header-section").innerHTML
                        videoType = videoType.toUpperCase()
                        if (videoType != "STEEZY") videoType = videoType.replace("S", "")
                        /*fetch("/users/videos/send", options1)
                            .then(response => response.json())
                            .then(videoPath =>{
                                if(videoPath != null){*/
                        let options = {
                            method: "POST",
                            headers: {
                                "Content-type": "application/json",
                                "Authorization": localStorage.getItem("token")
                            },
                            body: JSON.stringify({
                                "id": 0,
                                "description": data.video_description,
                                "tricks": tricksInVid,
                                "filesystemLocation": data.video,
                                "videoType": videoType

                            })
                        }

                        fetch("/users/" + myId + "/videos/post", options)
                            .then(response => {
                                if (response.status == 200) {
                                    window.location.reload()
                                }
                            })
                        /*
                }
            }
        )
        .catch(err => console.log("there has been an error man!MAN\n"+err))*/

                    }}>
                    <Form>
                        First, copy the YouTube video's url and paste it here:
                        <Field id="video-choice" as="input" name="video" placeholder="youtube url"></Field>
                        <br />

                        Describe your video if you want!
                        <Field id="add-video-description" as="textarea" name="video_description" placeholder="video description"></Field>
                        <br />
                        Choose tricks that are in the video(<span id="number-of-tricks-in-video-form">0</span>)
            <br />
                        <div id="add-video-tricks-div">
                            {tricks.map(trick =>
                                <div id={"add-video-trick-" + trick.id}>
                                    <Trick trick={trick} videoForm={true} />
                                </div>
                            )}
                        </div>
                        <br />
                                <br />
                        <button id="send-video" type="submit">send</button>
                    </Form>
                </Formik>
            </div>



        </div>);
}

export default AddForm;