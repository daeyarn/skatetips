import React from 'react'
import { Field, Form, Formik } from 'formik'
import "./AddTrickFormUser.css"
import Trick from "../../Components/Trick.js"

function AddTrickFormUser(props) {

    const [tricks, setTricks] = React.useState([]);

    React.useEffect(() => {
        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("/tricks", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(tricks => setTricks(tricks))
    }, [])

    return (
        <div>
            <div id="trick-user-form-div">

                <h3>Suggest a trick!</h3>
                <hr />
                <span>Suggest a trick to be added to the database! Have in mind the tricks that are already in the system.</span>
                <br />
                <br />
                <Formik initialValues={{ "trick_name": '', "trick_description": '' }}
                    onSubmit={(data, actions) => {
                        if (data.trick_name == '' || data.trick_description == '') {
                            alert("Please enter trick name and description.")
                            return;
                        }
                        let lowerTrickName = data.trick_name.toLowerCase().trim().replace(" ", "_");
                        let existingTricks = document.getElementsByClassName("trick-user-add-form");


                        if (document.getElementsByClassName(lowerTrickName).length != 0) {
                            console.log(lowerTrickName)
                            alert("This trick might already exist.")
                            return;
                        }

                        let options = {
                            method: "POST",
                            headers: {
                                "Content-type": "application/json",
                                "Authorization": localStorage.getItem("token")
                            },
                            body: JSON.stringify({
                                "description": data.trick_description,
                                "name": data.trick_name,
                                "stance": "FAKIE"
                            })
                        }

                        fetch("/tricks/suggest", options)
                            .then(response => console.log(response))

                        actions.resetForm();

                    }}>
                    <Form>
                        <Field as="textarea" name="trick_name" placeholder="trick name"></Field>
                        <br />
                        <Field as="textarea" name="trick_description" placeholder="trick description"></Field>
                        <br />
                        <button type="submit" id="add-trick-user-button">Send</button>
                    </Form>
                </Formik>
            </div>
            {tricks.length != 0 &&
            <div id = "existing-tricks-container">
             <h3>Existing tricks</h3>
                <div id="tricks-for-user-add-trick">
                   
                    {tricks.map(trick => 
                    <div className = "single-trick-add-trick-div">
                        <Trick trick={trick} idsAreNames={true} />
                    </div>
                    )}
                </div>
            </div>
            }
        </div>
    );

}

export default AddTrickFormUser;