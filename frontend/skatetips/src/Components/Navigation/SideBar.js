import React from 'react'
import "./SideBar.css"
import {Link} from 'react-router-dom'
 

function SideBar(props){
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let name = bearer.usrname;
    //if(name.length > 10) name = name.substr(0, 10)+"...";
    let role = bearer.role;
    
    let currentUserId = bearer.id;
    let selected = props.selected;

    let sections = ["interact", name, "requests", "followings", "reports", "logout"];
    if(role != "USER") sections.push("add-trick");

    React.useEffect(() => {
        document.getElementById("logout").addEventListener("click", function(){
            localStorage.removeItem("token")
        })
        
    }, [])

    return(
        <div className = "navigation">
            <Link to="/interact" className = "link-no-decoration">
            <div id = "interact" className = {"interact section " + (selected == "interact" ? "selected" : "")} >
                interact
            </div>
            </Link>
            <Link to={"/users/"+currentUserId} className = "link-no-decoration">
            <div id = "name" className = {"name section " + (selected == "name" ? "selected" : "")}>
                {name}
            </div>
            </Link>
            <Link to="/requests" className = "link-no-decoration">
            <div id = "requests" className = {"requests section " + (selected == "requests" ? "selected" : "")}>
                requests
            </div>
            </Link>
            <Link to="/followings" className = "link-no-decoration">
            <div id = "followings" className = {"followings section " + (selected == "followings" ? "selected" : "")}>
                followings
            </div>
            </Link>
            <Link to="/reports" className = "link-no-decoration">
            <div id = "reports" className = {"reports section " + (selected == "reports" ? "selected" : "")}>
                reports
            </div>
            </Link>
            
                <Link id = "add-trick" to="/add-trick" className = "link-no-decoration"> 
                    <div className = {"add-trick section " + (selected == "add-trick" ? "selected" : "")}> 
                        add trick    
                    </div>
                </Link>
            
            <Link to="/logout" className = "link-no-decoration">
                <div id = "logout" className = "logout section">
                    logout
                </div>
            </Link>

            <div className = "last-div">
        
            </div>
        </div>

    );
}

export default SideBar;