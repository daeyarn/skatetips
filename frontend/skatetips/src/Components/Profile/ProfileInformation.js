import React from 'react'
import ReactDOM from 'react-dom'
import "../Interaction/RandomSkater.css"
import { Link } from 'react-router-dom'
import "./ProfileInformation.css"
import Trick from "../../Components/Trick.js"
import { Form, Formik, Field } from 'formik'
import ChangeInfo from "../Profile/ChangeInfo.js"

function ProfileInformation(props) {
    //profile actions are: follow/unfollow, make requests, report (given that the profile is not the user's, then it will be update info :) :DDD)
    let myProfile = false;
    let requestFormActive = false;
    let reportFormActive = false;
    
    const [skaterInfo, setSkaterInfo] = React.useState(null);
    const [followingProfile, setFollowingProfile] = React.useState(false);
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let userId = bearer.id;
    let profileId = parseInt(props.id);

    if (userId === profileId) myProfile = true;
    React.useEffect(() => {
        let options1 = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }

        fetch("/users/" + profileId + "/info", options1)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(skaterInfo => {
                setSkaterInfo(skaterInfo)
                
                /*setTimeout(e => {
                document.getElementById("profile-info-description").style.width = "100%"
            }, 500)
                */
                let wantsHelp = skaterInfo.wantsHelp;
                if (myProfile == false && wantsHelp != true) {
                    let illegalSelection = document.getElementById("request-form-offer-help-selection")
                    illegalSelection.parentNode.removeChild(illegalSelection);
                }
               
                console.log(skaterInfo.profilePicture)
                if(skaterInfo.profilePicture != null){
                    console.log(skaterInfo.profilePicture)
                    document.getElementById("random-skater-photo-div").style.backgroundColor = skaterInfo.profilePicture;
                }
            })
            .catch(error => console.log(error))
            /*
            if(myProfile != true) return
            document.getElementById("username-fixed-div").addEventListener('click', (e) =>{
                console.log(document.getElementById("username-fixed-div").style.height)
                if(document.getElementById("skater-name-h1") == null) return
                let oldName = document.getElementById("skater-name-h1").innerHTML
                let changeName = 
                <div id = "change-name-form-div">
                <Formik className = "change-data-info-form"
                    initialValues = {{name: ""}}
                    onSubmit = {(data, actions) => {
                        if(data.name == ""){
                            alert("Name cannot be blank.")
                            actions.resetForm()
                            return
                        }

    
    
                    }}>
                    <Form>
                        <Field name = "name" placeholder = "new name"/>
                        <div className = "change-name-form-div-buttons-div">
                        <button className = "change-name-form-div-button change-name-form-div-button-chng" 
                        onClick = {() => {
                            alert("That's not cool")
                        }}>change</button>
                        <button className = "change-name-form-div-button change-name-form-div-button-cancel" 
                        onClick = {() => {
                            ReactDOM.render(
                            <div>
                                <h1 id="skater-name-h1">{oldName}</h1>
                                <hr/>
                            </div>
                            , document.getElementById("username-fixed-div"))
                        }}>cancel</button>
                        </div>
                    </Form>
                </Formik>
                
                <hr/>
                </div>
                
                ReactDOM.render(changeName, document.getElementById("username-fixed-div"))
            })*/
            
        if (myProfile == true){
            document.getElementById("change-info-button").addEventListener("click", function(){
                if(document.getElementById("change-info-button").innerHTML != "Update info" && document.getElementById("change-info-button").innerHTML != "Videos") return
                if(document.getElementById("change-info-button").innerHTML == "Update info"){
                let node = <ChangeInfo myId = {userId}/>
               
                
                ReactDOM.render(node, document.getElementById("main-div-video-segment")); 
                document.getElementById("change-info-button").innerHTML = "Videos"

                document.getElementById("video-input-div").style.marginTop = "100vh"
                
                }
                else{
                    window.location.reload();
                }
                                    
            })
            return;
        }

        let options2 = {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify({
                "skaterId": userId
            }),
        }

        //console.log(options)
        fetch("/users/" + profileId + "/following", options2)
            .then(response => response.json())
            .then(followingProfileBool => {
                console.log(followingProfileBool)
                if (document.getElementById("follow-button") == null) return;
                if (followingProfileBool == true) document.getElementById("follow-button").innerHTML = "Unfollow";
                else document.getElementById("follow-button").innerHTML = "Follow";
                document.getElementById("follow-button").disabled = false;
            })



        document.getElementById("follow-button").addEventListener("click", followUser);

        document.getElementById("request-button").addEventListener('click', function () {
            if (reportFormActive) {
                document.getElementById("report-popup-div").style.marginLeft = "140vw";
                reportFormActive = false;
            }

            if (requestFormActive) {
                document.getElementById("request-popup-div").style.marginLeft = "120vw";
                requestFormActive = false;
            }

            else {
                document.getElementById("request-popup-div").style.marginLeft = "59vw";
                requestFormActive = true;
            }
        })

        if(bearer.role == "ADMIN"){
            document.getElementById("delete-account-button").addEventListener('click', function(){
                if(window.confirm("Are you sure you want to delete this user's profile?")){
                    fetch("/users/"+profileId+"/delete", 
                    {
                        method: "DELETE",
                        headers: {
                            "Content-type": "application/json",
                            "Authorization": localStorage.getItem("token")
                        }
                    }).then(response => {
                        if(response.status == 200)
                            alert("Profile successfully deleted.")
                            window.location = "/interact"
                    })    
                }

            })
            return;
        }
        document.getElementById("report-b").addEventListener('click', function () {
            if (requestFormActive) {
                document.getElementById("request-popup-div").style.marginLeft = "120vw";
                requestFormActive = false;
            }

            if (reportFormActive) {
                document.getElementById("report-popup-div").style.marginLeft = "140vw";
                reportFormActive = false;
            }

            else {
                document.getElementById("report-popup-div").style.marginLeft = "59vw";
                reportFormActive = true;
            }


        })


    }, [])

    function followUser() {
        let btnText = document.getElementById("follow-button").innerHTML
        if(btnText != "Follow" && btnText != "Unfollow") return
        let httpMethod = "POST"
        if (btnText == "Unfollow") httpMethod = "PUT";
        console.log("yas")
        let options = {
            method: httpMethod,
            headers: {
                "Content-type": "application/json",
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify({
                "skaterId": userId
            }),
        }
        console.log(options)

        let url = "/users/" + profileId + "/" + btnText.toLowerCase()
        fetch(url, options)
            .then(response => console.log(response))

        if (btnText == "Follow") {
            document.getElementById("follow-button").innerHTML = "Unfollow"
            let followersCount = parseInt(document.getElementById("followers-info").innerHTML)
            console.log(followersCount)
            followersCount++;
            document.getElementById("followers-info").innerHTML = followersCount
        }
        else if (btnText == "Unfollow") {
            document.getElementById("follow-button").innerHTML = "Follow"
            let followersCount = parseInt(document.getElementById("followers-info").innerHTML)
            followersCount--;
            document.getElementById("followers-info").innerHTML = followersCount
        }

    }

    return (

        <div className="profile-info-div">
            <div id = "random-skater-photo-div" className="random-skater-photo-div">
            </div>
            <div id = "username-fixed-div" className = "username-fixed-div">
                <h1 id="skater-name-h1">{skaterInfo == null ? <span><i></i></span> : skaterInfo.username}</h1>
                <hr />
                </div>
            <div className="random-skater-description-div">
                <br/>
                <br/>
                <br/>
                <span><em>stance</em>: {skaterInfo == null ? <span><i></i></span> : skaterInfo.stance.toLowerCase()}</span>
                <br />
                <span><em>followers</em>: {skaterInfo == null ? <span><i></i></span> : <span  id = "followers-info">{skaterInfo.numberOfFollowers}</span>}</span>
                <br />
                {/*<span><em>kudos</em>: {skaterInfo == null ? <span><i></i></span> : skaterInfo.kudos}</span><br />*/}
                
                <span><em>wants help</em>: {skaterInfo == null ? <span></span> : <span id="wants-help-span"> {skaterInfo.wantsHelp == true ? "yes" : "no"} </span>}</span>
                <hr />

                <div id = "profile-info-description" className="scrollable">
                    {skaterInfo == null ? <em>no description</em> : skaterInfo.description}
                </div>
                <hr />
                <h3><em>known tricks: </em></h3> {skaterInfo == null || skaterInfo == undefined ? <span></span>
                    :
                    skaterInfo.known.length == 0 ? <span><em>no tricks here</em></span>
                        :
                        skaterInfo.known.map(trick => <Trick key={trick} trick={trick} />)
                }
                <br />
                <h3><em>learning tricks: </em></h3> {skaterInfo == null ? <span></span>
                    :
                    skaterInfo.learning.length == 0 ? <span><em>no tricks here</em></span>
                        :
                        skaterInfo.learning.map(trick => <Trick key={trick} trick={trick} />)
                }
            </div>

            {userId == profileId ?
                <div id="single-button-change-info-div" className="profile-actions-div">
                    <button id="change-info-button" className="change-info">Update info</button>
                </div>
                :
                <div className="profile-actions-div">
                    <div className="profile-button-div">

                        <button id="request-button" className="button-profile">Request</button>

                    </div>
                    <div className="profile-button-div">
                        <button id="follow-button" className="button-profile" disabled>Follow</button>
                    </div>
                    {bearer.role != "ADMIN" ? 
                    <div className="profile-button-div">

                        <button id="report-b" className="button-profile">Report</button>

                    </div>
                    :
                    <div className="profile-button-div">

                        <button id="delete-account-button" className="button-profile" color = "red">Delete profile</button>

                    </div>

            }
                </div>
            }
        </div>




    );


}

export default ProfileInformation;