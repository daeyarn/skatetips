import React from 'react';
import Trick from "../Trick.js"
import { Field, Formik, Form } from 'formik'
import { Tooltip } from '@varld/popover'
import "./ChangeInfo.css"


function ChangeInfo(props) {

    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let userId = bearer.id;

    localStorage.setItem("learningTricks", "")
    localStorage.setItem("knownTricks", "")
    let profileId = parseInt(props.myId)
    console.log(profileId)
    let [skaterInfo, setSkaterInfo] = React.useState(null)
    let [tricks, setTricks] = React.useState([])
    let removePic = false
    let removeDesc = 0
    React.useEffect(() => {

        document.getElementById("color-selector").addEventListener("change", e => {
            document.getElementById("random-skater-photo-div").style.backgroundColor = document.getElementById("color-selector").value
        })
        fetch("/tricks")
            .then(response => {
                if (response.status == 403) {
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(tricks => setTricks(tricks))

        document.getElementById("delete-profile-button").addEventListener("click", function () {

            if (window.confirm("Are you sure you want to delete your profile? This action is irreversible.")) {
                fetch("/users/" + profileId + "/delete",
                    {
                        method: "DELETE",
                        headers: {
                            "Content-type": "application/json",
                            "Authorization": localStorage.getItem("token")
                        }
                    }).then(response => {

                        if (response.status == 403) {
                            alert("Session has expired. Please log in again to continue.")
                            document.getElementById("logout").click()
                            return;
                        }

                        console.log(response)
                        if (response.status == 200) {
                            document.getElementById("logout").click()
                            return
                        }
                    })
            }
            else return;
        })

        fetch("/users/" + profileId + "/info",
            {
                headers: {
                    "Authorization": localStorage.getItem("token")
                }
            })
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(skaterInfo => {
                setSkaterInfo(skaterInfo)
                //console.log(profileId, skaterInfo.learning)
                let learnTricks = ""
                for (let i = 0; i < skaterInfo.learning.length; i++) {
                    document.getElementById(skaterInfo.learning[i].id + "learn").style.marginLeft = "0%";
                    //console.log(document.getElementById(skaterInfo.learning[i].id+"learn"))
                    learnTricks += skaterInfo.learning[i].id + "learn"
                }
                localStorage.setItem("learningTricks", learnTricks)

                let knownTricks = ""
                for (let i = 0; i < skaterInfo.known.length; i++) {
                    document.getElementById(skaterInfo.known[i].id + "known").style.marginLeft = "50%";
                    knownTricks += skaterInfo.known[i].id + "known"
                }

                localStorage.setItem("knownTricks", knownTricks)

                //document.getElementById("registration-input-username").value = skaterInfo.username
                //document.getElementById("registration-input-select").value = skaterInfo.stance.toLowerCase()
                //document.getElementById("registration-input-description").value = skaterInfo.description



                document.getElementById("remove-desc").addEventListener("click", function () {
                    console.log(removeDesc)
                    removeDesc = 1 - removeDesc;
                    removeDesc == 1 ? document.getElementById("remove-desc").style.color = "green" : document.getElementById("remove-desc").style.color = "red"
                })
            })
            .catch(error => console.log(error))
    }, [])
    return (
        <div>
            <div id="change-info">
                <div>
                    <Formik className="registration-form-div"
                        initialValues={{
                            username: "",
                            stance: skaterInfo == null ? null : skaterInfo.stance,
                            description: null,
                            wantsHelp: null,
                            profilePic: null,
                            known: [],
                            learning: [],
                            color: null
                        }}
                        onSubmit={(data, actions) => {
                            //document.getElementById("color-selector").value

                            if (data.description != null && data.description.replace(" ", "").length > 100) {
                                alert("Description too long. Please use 100 characters max.")
                                return
                            }

                            if (data.username != "" && data.username.length > 20) {
                                alert("Username too long. Please use 20 characters max.")
                                return;
                            }
                            console.log(data.color)

                            let knownTricks = localStorage.getItem("knownTricks");
                            let learningTricks = localStorage.getItem("learningTricks");

                            if (knownTricks == "") knownTricks = [];
                            else {
                                knownTricks = knownTricks.split("known")
                                knownTricks.pop()
                                let knownTricksTmp = []
                                for (let i = 0; i < knownTricks.length; i++) {
                                    knownTricksTmp.push(parseInt(knownTricks[i]))
                                }

                                knownTricks = knownTricksTmp;
                            }



                            if (learningTricks == "") learningTricks = []
                            else {
                                learningTricks = learningTricks.split("learn")
                                learningTricks.pop()
                                let learningTricksTmp = []
                                for (let i = 0; i < learningTricks.length; i++) {
                                    learningTricksTmp.push(parseInt(learningTricks[i]))
                                }

                                learningTricks = learningTricksTmp;
                            }


                            let d = document.getElementById("remove-desc").style.color == "green" ? "" : data.description != null && data.description != "" ? data.description : skaterInfo.description
                            //console.log(data.color, skaterInfo.profilePic)
                            let options = {
                                method: "POST",
                                headers: {
                                    "Content-type": "application/json",
                                    "Authorization": localStorage.getItem("token")
                                },
                                body: JSON.stringify({
                                    "username": data.username.replace(" ", "") == "" ? skaterInfo.username : data.username,
                                    "stance": data.stance == null ? skaterInfo.stance : data.stance,
                                    "description": d,
                                    "wantsHelp": data.wantsHelp == null ? skaterInfo.wantsHelp : data.wantsHelp,
                                    "knownTricks": knownTricks,
                                    "learningTricks": learningTricks,
                                    "profilePictureLocation": data.color == null ? skaterInfo.profilePicture : data.color
                                }),

                            }


                            fetch("/users/" + userId + "/info/update", options)
                                .then(response => {
                                    if (response.status == 200) {
                                        document.getElementById("logout").click();
                                        return;
                                    }
                                    else if (response.status == 400) {
                                        alert("The given username is already in use by another user that is not you.")
                                    }
                                })

                            //actions.resetForm();

                        }
                        }>
                        <Form className="registration-form">
                            <div id="update-bordered-div" className="update-info-bordered-div">
                                <h1>Change your profile info!</h1> (on clicking save changes you will be logged out and need to log in again to see changes)
                                <hr />

                                <table className="info-input-form-table">
                                    <tr>

                                        <td>

                                            <span className="white-text">username</span>

                                        </td>
                                        <td>

                                            <Field maxlength="20" id="registration-input-username" className="registration-input" as="input" name="username" placeholder="username" />

                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <span className="white-text">your stance</span>
                                        </td>
                                        <td>
                                            <Field id="registration-input-select" className="registration-input" as="select" name="stance">
                                                <option id="no-display-option" value="stance">stance</option>
                                                <option name="goofy" value="GOOFY">goofy</option>
                                                <option name="regular" value="REGULAR">regular</option>
                                            </Field>
                                        </td>

                                    </tr>
                                    <tr>
                                        <td>
                                            <span className="white-text">do you want help </span>
                                        </td>
                                        <td>
                                            <Field className="registration-input" as="select" name="wantsHelp">
                                                <option id="no-display-option" value="stance">help</option>
                                                <option value={true}>yes</option>
                                                <option value={false}>no</option>
                                            </Field>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>

                                            <button id="remove-desc" type="button" className="remove-some-info">X</button>

                                            <span className="white-text">description </span>
                                        </td>
                                        <td>
                                            <Field rows="10" maxlength="100" id="registration-input-description" className="registration-input" as="textarea" name="description" placeholder="description" rows="1" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span className="white-text">profile picture background</span>
                                        </td>
                                        <td>
                                            <Field id="color-selector" type="color" name="color"></Field>
                                        </td>
                                    </tr>

                                </table>

                                <div className="tricks">
                                    <div>
                                        <div className="registration-header-div">
                                            <h1 className="registration-h1" id="tyk">Known tricks</h1>
                                        </div>
                                        <div className="registration-header-div">
                                            <h1 className="registration-h1" id="tyw">Learning tricks</h1>
                                        </div>
                                    </div>
                                    <div className="known-tricks">
                                        <div className="all-tricks">
                                            {tricks.map(trick =>

                                                <div id={trick.id + "known"} className="trick-with-button">
                                                    <Trick learning={false} divId={trick.id + "known"} trick={trick} />
                                                    {/*console.log(trick.id + "known")*/}
                                                </div>
                                            )}


                                        </div>
                                        <div className="chosen-tricks">

                                        </div>
                                    </div>

                                    <div className="known-tricks">
                                        <div className="all-tricks-right">
                                            {tricks.map(trick =>
                                                <div id={trick.id + "learn"} className="trick-with-button-right">
                                                    <Trick learning={true} divId={trick.id + "learn"} trick={trick} />

                                                </div>
                                            )}
                                        </div>
                                        <div className="chosen-tricks">

                                        </div>
                                    </div>
                                </div>
                                <label id="update-failed-warning"></label>
                                <button id="save-changes-info" type="submit">Save changes</button>
                            </div>
                        </Form>


                    </Formik>
                </div>
            </div>
            <hr />
            <table id="bottom-half-table">
                <tr >
                    <td className="bottom-half-change-info-td">
                        <div id="change-password">
                            <div id="change-password-inner-div">
                                <Formik initialValues={
                                    {

                                        new_password: "",
                                        repeated_new_password: ""
                                    }
                                }
                                    onSubmit={(data, actions) => {
                                        if (data.new_password == "") {
                                            alert("Password cannot be blank.")
                                            return;
                                        }

                                        if (data.new_password != data.repeated_new_password) {
                                            alert("Passwords do not match.")
                                            return;
                                        }

                                        if (data.new_password.length < 8) {
                                            alert("Password needs to be at least 8 characters long.")
                                            return;
                                        }

                                        if (data.new_password.length > 255) {
                                            alert("Password too long. Please make it 255 characters max.")
                                            return;
                                        }
                                        fetch("/users/" + profileId + "/password/update",
                                            {
                                                method: "POST",
                                                headers: {
                                                    "Content-type": "application/json",
                                                    "Authorization": localStorage.getItem("token")
                                                },
                                                body: JSON.stringify({
                                                    "password": data.new_password
                                                }),
                                            }).then(response => {
                                                console.log(response.status)
                                                if (response.status == 200) {
                                                    document.getElementById("logout").click();
                                                }
                                            })
                                    }}>
                                    <Form>
                                        <table className="change-password-table">
                                            <b>Change password.</b>
                                            <tr>
                                                <td>
                                                    new password
                                                </td>
                                                <td>
                                                    <Field maxlength="255" type="password" name="new_password" placeholder="new password"></Field>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    repeat new
                                                </td>
                                                <td>
                                                    <Field maxlength="255" type="password" name="repeated_new_password" placeholder="repeat new"></Field>
                                                </td>

                                            </tr>
                                        </table>
                                        <div id="save-changes">
                                            <button id="save-changes-button" type="submit">Save changes</button>
                                        </div>


                                    </Form>
                                </Formik>
                            </div>

                        </div>
                    </td>
                    <td className="bottom-half-change-info-td">
                        <div id="delete-own-profile">
                            <b>Deactivate profile.</b>
                            <br />
                            <span>Deactivating your profile will delete all your information,
                videos and remove you from all friend lists. This action cannot be undone.</span>
                            <button id="delete-profile-button">Delete profile</button>
                        </div>
                    </td>
                </tr>
            </table>

        </div>

    );
}

export default ChangeInfo;