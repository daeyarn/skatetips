import React from 'react'
import "../../Components/Form/InteractionForm.css"
import "./VideoSegment.css"
import VideoList from "../VideoList.js"
import { Tooltip } from '@varld/popover';

function VideoSegment(props) {
    let profileId = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);

    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let myId = bearer.id;
    let ownProfile = false;
    if (profileId == "my-profile") {
        ownProfile = true;
        profileId = myId;
    }

    /*
    React.useEffect( e => {
        let elements = document.getElementsByClassName("videos-div")
        for(let i=0; i<elements.length; i++){
            elements[i].style.height = "33%"
        }
    }, [])
    */
    profileId = parseInt(profileId)
    return (
        <div id = "main-div-video-segment" className="main-div main-div-video-segment">
            <div className="videos-div tutorials">
                <div className="fixed-header-div">Tutorials(<span id = "tutorials-number">0</span>)
                {ownProfile && <Tooltip display = "inline" content = {"Add video to tutorials."}><div id = {"add-tutorials-video"} className = "add-video-div"></div></Tooltip>}
                </div>

                <VideoList videos="tutorials" profileId={profileId}/>

            </div>
            <div className="videos-div tries">
                <div className="fixed-header-div">Tries(<span id = "trials-number" >0</span>)
                {ownProfile && <Tooltip display = "inline" content = {"Add video to tries."}><div id = {"add-trials-video"} className = "add-video-div"></div></Tooltip>}</div>

                <VideoList videos="trials" profileId={profileId}/>

            </div>
            <div className="videos-div steezy">
                <div className = "fixed-header-div">Steezy(<span id = "steezy-number">0</span>)
                {ownProfile && <Tooltip display = "inline" content = {"Add video to steezy."}><div id = {"add-steezy-video"} className = "add-video-div"></div></Tooltip>}</div>
                <VideoList videos="steezy" profileId={profileId}/>

            </div>
        </div>
    );
}

export default VideoSegment;