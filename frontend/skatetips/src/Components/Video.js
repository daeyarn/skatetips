import React from 'react'
import "./Video.css"
import Trick from "../Components/Trick.js"
import { ToolTip } from "@varld/popover";
import ReactDOM from 'react-dom'

function Video(props) {
    console.log(props.video.description)
    console.log(props.video)
    let selected = false;
    let src = props.video.filesystemLocation.replace("watch?v=", "embed/");

    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let myId = bearer.id;

    let profileId = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
    let btn = null;
    let myProfile = profileId == "my-profile";
    let btnText = myProfile == true ? "delete" : "like";
    let oldVideo = null

    React.useEffect(() => {
        /*
        btn = document.createElement("button")
        btn.id = myProfile ? "delete-video-" + props.video.id : "like-video-" + props.video.id
        if (myProfile) {
            btn.innerHTML = "delete";
            btn.addEventListener('click', function () {
                if (window.confirm("Are you sure you want to delete this video?")) {
                    btn.disabled = true;
                    window.location.reload();
                }
            })
        }
        else {

        } 
        document.getElementById("video-" + props.video.id + "-description").appendChild(btn)
        */

        document.getElementById("single-video-" + props.video.id + "-div").addEventListener("click", function () {
            let allVideos = document.getElementsByClassName("single-video-div");
            /*for (let i = 0; i < allVideos.length; i++) {
                allVideos[i].style.width = "20vw";
            }*/
            if (selected == false) {
                document.getElementById("single-video-" + props.video.id + "-div").style.width = "98%";
                selected = true;
                //document.getElementById("single-video-" + props.video.id + "-action").style.display = "block";


                if(myProfile != true) return;
                let container = document.getElementById("video-"+props.video.id+"-image-div");
                let dltDiv = 
                <div id = {"single-video-"+props.video.id+"-action"} className = "single-video-action">
                </div>

                
            //<button className="single-video-action" id={"single-video-" + props.video.id + "-action"}>{btnText}</button>

                
                ReactDOM.render(dltDiv, container)

                document.getElementById("single-video-"+props.video.id+"-action").addEventListener('click', (e) => {
                    if(window.confirm("Are you sure you wish to delete this video?")){
                        let options = {
                            method: "DELETE",
                            headers: {
                                "Content-type": "application/json",
                                "Authorization": localStorage.getItem("token")
                            },
                            body: JSON.stringify({
                                "id": props.video.id,
                                "description": props.video.description,
                                "tricks": props.video.tricks,
                                "filesystemLocation": props.video.filesystemLocation,
                                "videoType": props.video.videoType
                            })
                        }

                        fetch("/users/"+myId+"/videos/delete", options)
                            .then(response => {
                                
                                    if(response.status == 403){
                                        alert("Session has expired. Please log in again to continue.")
                                        document.getElementById("logout").click()
                                        return;
                                    }
                                    
                                
                                response.json()
                                let exVideo = document.getElementById("single-video-"+props.video.id+"-div")
                                exVideo.style.opacity = "0"
                                exVideo.style.marginTop = "-10vh"
                                
                                let videoTypeSpan = ""
                                switch(props.video.videoType){
                                    case "STEEZY": videoTypeSpan = "steezy"; break;
                                    case "TUTORIAL": videoTypeSpan = "tutorials"; break;
                                    case "TRIAL": videoTypeSpan = "trials"; break;
                                }
                                let thatSpan = parseInt(document.getElementById(videoTypeSpan+"-number").innerHTML)
                                thatSpan--;
                                document.getElementById(videoTypeSpan+"-number").innerHTML = thatSpan;
                                
                                setTimeout(e => {
                                    exVideo.parentElement.removeChild(exVideo)
                                }, 500)
                            })
                    }
                })
                
                return
            }
            if (selected == true) {
                
                document.getElementById("single-video-" + props.video.id + "-div").style.width = "20vw";
                selected = false;
                //document.getElementById("single-video-" + props.video.id + "-action").style.display = "block";

                if(myProfile != true) return;
                setTimeout(e => {
                    let container = document.getElementById("video-"+props.video.id+"-image-div");
                    let oldVideo = <iframe allowfullscreen="true" className="video-iframe" id={"video-" + props.video.id} src={src}></iframe>

                ReactDOM.render(oldVideo, container)
                }, 500)
                
                
                return
            }
        })

    }, [])
    return (
        <div id={"single-video-" + props.video.id + "-div"} className="single-video-div">
            <div id={"video-" + props.video.id + "-image-div"} className="thumbnail-div">
                <iframe allowfullscreen="true" className="video-iframe" id={"video-" + props.video.id} src={src}></iframe>
            </div>
            <div id={"video-" + props.video.id + "-description"} className="video-description-div">
                
            <div className = "video-description">
                    <span id={"video-" + props.video.id + "-description"}>{props.video.description != "" ? props.video.description : <em>no description</em>}</span>
                    <hr />
                </div>
                <div className="video-present-tricks" id={"video-" + props.video.id + "-present-tricks"}>
                    {props.video.tricks.length == 0 ? <em>no tricks specified</em> : props.video.tricks.map(trick => <Trick trick={trick} />)}
                </div>
            </div>
            
        </div>
    );
}

export default Video;