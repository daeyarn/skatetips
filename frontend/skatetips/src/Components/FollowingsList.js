import React from 'react'
import Following from "./Following.js"
import "./FollowingsList.css"

function FollowingsList(props){
   

    let followings_to_map = []
    if(props.flag === "followers"){
        for(let i=0; i<props.followings.length; i++)
            if(props.followings[i].followType === "follower")
                followings_to_map.push(props.followings[i])
        
        document.getElementById("followers-header-span").innerHTML = "Followers("+followings_to_map.length+")"
    }

    else if(props.flag === "followings"){
        for(let i=0; i<props.followings.length; i++)
            if(props.followings[i].followType === "following")
                followings_to_map.push(props.followings[i])
        
        document.getElementById( "following-header-span").innerHTML = "Following("+followings_to_map.length+")"
    }
    return(
        <div className = "followings-list-main-div">
            {followings_to_map.length != 0 && followings_to_map.map(following => <Following following = {following} />)}
        </div>
    )
}

export default FollowingsList;