import React from 'react'
import "./TrickSuggestion.css"

function TrickSuggestion(props) {

    React.useEffect(() => {

        document.getElementById("put-in-form-trick-suggestion-" + props.trick.id).addEventListener("click", function () {
           
            let name = props.trick.name
            let description = props.trick.description

            let lowerTrickName = name.toLowerCase().trim().replace(" ", "_");

            if (document.getElementsByClassName(lowerTrickName).length == 0) 
                document.getElementById("delete-trick-suggestion-" + props.trick.id).click();

            document.getElementById("delete-trick-suggestion-" + props.trick.id).disabled = true;
            document.getElementById("put-in-form-trick-suggestion-" + props.trick.id).disabled = true;

            

            if (document.getElementsByClassName(lowerTrickName).length != 0) {
                console.log(lowerTrickName)
                alert("This trick might already exist.")
                document.getElementById("delete-trick-suggestion-" + props.trick.id).disabled = false;
                document.getElementById("put-in-form-trick-suggestion-" + props.trick.id).disabled = false;
                return;
            }

            
            document.getElementById("add-trick-admin-form-name").value = name
            document.getElementById("add-trick-admin-form-description").value = description

            let stances = ["FAKIE", "NATURAL", "SWITCH", "NOLLIE"]
            for (let i = 0; i < 4; i++) {
                let options = {
                    method: "POST",
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": localStorage.getItem("token")
                    },
                    body: JSON.stringify({
                        "description": description,
                        "name": name,
                        "stance": stances[i]
                    })
                }

                fetch("/tricks/add", options)
                    .then(response => {
                        console.log(response)
                        
                            if(response.status == 403){
                                alert("Session has expired. Please log in again to continue.")
                                document.getElementById("logout").click()
                                return;
                            }
                            
                        if (response.status != 200) {
                            console.log("Something went wrong.");
                        }
                        else if(i==3){
                            window.location.reload();
                        }
                    })
            }

            
            

        })


        document.getElementById("delete-trick-suggestion-" + props.trick.id).addEventListener("click", function () {

            document.getElementById("delete-trick-suggestion-" + props.trick.id).disabled = true;
            document.getElementById("put-in-form-trick-suggestion-" + props.trick.id).disabled = true;
            let options = {
                method: "DELETE",
                headers: {
                    "Content-type": "application/json",
                    "Authorization": localStorage.getItem("token")
                }
            }

            fetch("/tricks/suggestions/" + props.trick.id + "/delete", options)
                .then(response => {
                    console.log(response)
                    if (response.status != 200) window.location.reload();
                    let exSuggestion = document.getElementById("single-trick-suggestion-div-" + props.trick.id)
                    exSuggestion.parentNode.removeChild(exSuggestion);
                    let newNumberOfSuggestions = parseInt(document.getElementById("number-of-suggestions").innerHTML)
                    newNumberOfSuggestions--;
                    document.getElementById("number-of-suggestions").innerHTML = newNumberOfSuggestions;

                })


        })
    }, [])
    return (
        <div className="single-trick-suggestion-div" id={"single-trick-suggestion-div-" + props.trick.id}>
            <h3 className="suggestion-header-h3">Suggestion</h3>
            <hr />
            <span id = {"suggestion-name-"+props.trick.id}>{props.trick.name}</span>
            <br />
            <span id = {"suggestion-description-"+props.trick.id}>{props.trick.description}</span>
            <br />
            <br />
            <button className="trick-suggestion-button put-suggestion" id={"put-in-form-trick-suggestion-" + props.trick.id}>Put</button>

            <button className="trick-suggestion-button delete-suggestion" id={"delete-trick-suggestion-" + props.trick.id}>Delete</button>

        </div>

    )





}

export default TrickSuggestion;