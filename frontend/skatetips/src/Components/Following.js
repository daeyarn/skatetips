import React from 'react'
import "./Following.css"
import {Link} from 'react-router-dom'

function Following(props){

    let id = props.following.userId;
    let name = props.following.userName;

    return(
        <div className = "single-following-div">
            <Link to = {"/users/"+id} className = "link-no-decoration">
                <h1>{name}</h1>
            </Link>

        </div>

    );

}

export default Following;