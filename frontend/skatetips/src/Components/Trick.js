import React from 'react';
import { Tooltip } from '@varld/popover';
import "./Trick.css"

function Trick(props) {
    let {id, description, name, stance } = props.trick;
    let divId = props.divId;
    let learning = props.learning;

    React.useEffect(() => {
        if(props.interact == true){
            document.getElementById("single-trick-"+id + "trick").addEventListener('click', function(){
                let oldSelected = localStorage.getItem("trickRequest")
                if(oldSelected == (id+"trick")){
                    localStorage.setItem("trickRequest", "")
                    document.getElementById("single-trick-"+id +"trick").style.borderColor = "white";
                    return;
                }
                document.getElementById("single-trick-"+id +"trick").style.borderColor = "blue";
                localStorage.setItem("trickRequest", id+"trick");
                if(oldSelected == "") return;
                document.getElementById("single-trick-" + oldSelected).style.borderColor = "white";
            })
        }
        if(document.getElementById(divId) == null) return;
        document.getElementById(divId).addEventListener('click', function(){
            
            if (learning) {
                let learningTricks = localStorage.getItem("learningTricks");
                if(learningTricks.includes(divId)){
                    learningTricks = learningTricks.replace(divId, "");
                    document.getElementById(divId).style.marginLeft = "50%";
                    localStorage.setItem("learningTricks", learningTricks);
                }
                else{
                    learningTricks += divId;
                    document.getElementById(divId).style.marginLeft = "0%";
                    localStorage.setItem("learningTricks", learningTricks);
        
                }
                console.log("Known: " +  localStorage.getItem("knownTricks") + "\nLearning: " + localStorage.getItem("learningTricks"))
            }
        
            if (!learning){
                let knownTricks = localStorage.getItem("knownTricks");
                if(knownTricks.includes(divId)){
                    knownTricks = knownTricks.replace(divId, "");
                    document.getElementById(divId).style.marginLeft = "0%";
                    localStorage.setItem("knownTricks", knownTricks);
                }
                else{
                    knownTricks += divId;
                    document.getElementById(divId).style.marginLeft = "50%";
                    localStorage.setItem("knownTricks", knownTricks);
        
                }
                console.log("Known: " +  localStorage.getItem("knownTricks") + "\nLearning: " + localStorage.getItem("learningTricks"))
            }


        });
    
    }, [])
   
    return (
        <div id = {props.videoForm == true ? "single-trick-in-video-"+id+"trick" : props.idsAreNames == true ? name.replace(" ", "_")+id : "single-trick-"+id+"trick"} className={"single-trick-div trick-user-add-form " + name.toLowerCase().replace(" ", "_")}>
            <Tooltip id = {"tooltip-trick-description-"+id} content={description} display="inline">
                <span id = {"name-and-stance-span-"+id}>{name}, {stance}</span>
            </Tooltip>
        </div>
    );
}

export default Trick;