import React from 'react'
import Request from "./Request.js"

function RequestList(props){
    //console.log(props.requests)
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let user = bearer.id;
    let flag = props.flag
    let requests_to_map = []
    if(flag === "received"){
        for(let i=0; i<props.requests.length; i++)
            if(props.requests[i].receiverId == user && props.requests[i].status != "REFUSED")
                requests_to_map.push(props.requests[i])
        document.getElementById("received-requests-header-span-count").innerHTML = requests_to_map.length;
    }

    else if(flag === "sent"){
        for(let i=0; i<props.requests.length; i++)
            if(props.requests[i].senderId == user)
                requests_to_map.push(props.requests[i])

        document.getElementById("sent-requests-header-span-count").innerHTML = requests_to_map.length;
    }

    return(
        <div>
            {requests_to_map.length != 0 && requests_to_map.map(help_request => <Request request = {help_request}/>)}
        </div>
    );
}

export default RequestList;