import React, {useEffect, useState} from "react";
import {Redirect, useHistory, Link} from 'react-router-dom'
import "./RandomSkater.css"
import { render } from "@testing-library/react";

function RandomSkater(props){
    let description = props.skater == undefined ? "no description" : props.skater.description;
    let usrname = props.skater == undefined ? "" : props.skater.username;
    
    let backgroundImage = props.skater.profilePicture != null ? "url("+props.skater.profilePicture+");" : "url('../../Images/mystery_random_skater.png');";
    let bgImg = {
        "background-image": backgroundImage
    }
    useEffect(() => {
        let randomSkaterDiv = document.getElementById("random-skater-div")
        randomSkaterDiv.style.marginLeft = "0vw";
        document.getElementById("random-skater-photo-div").style.backgroundColor = props.skater.profilePicture;
        /*
        console.log(props.skater.profilePicture)
        if(props.skater.profilePicture != null){
            document.getElementById("random-skater-photo-div").style.backgroundImage = "url("+props.skater.profilePicture+");"
        }*/
    }, [])
    return( 
    <div id = "random-skater-div" className = "random-skater-div">
        <div id = "random-skater-photo-div" className = "random-skater-photo-div" style = {bgImg}>
        </div>
        <div className = "random-skater-description-div">
            <h1 id = "skater-name-h1">{usrname}</h1>
            <hr/>
            <div id = "random-skater-description-div" className = "scrollable">
            {description}
            </div>
        </div>
        {props.skater && 
        <div className = "visit-profile-div">
            <Link to={"/users/"+props.skater.id} className = "link-no-decoration">
                <button id = "visit-profile-button" type="submit">Visit profile</button>
            </Link>
        </div>}
        


    </div>
    );
}

export default RandomSkater;