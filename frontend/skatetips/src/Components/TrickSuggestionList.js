import React from 'react'
import TrickSuggestion from './TrickSuggestion.js'
import "./TrickSuggestionList.css"

function TrickSuggestionsList(props){
    const [suggestions, setSuggestions] = React.useState([])

    React.useEffect(() => {
        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("/tricks/suggestions", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(suggestions => {
                
                setSuggestions(suggestions)
                document.getElementById("number-of-suggestions").innerHTML = suggestions.length;
            })

    }, [])

    return(
        <div id = "trick-suggestions-div">
            {suggestions.map(suggestion => <TrickSuggestion trick = {suggestion}/>)}

        </div>
    );

}

export default TrickSuggestionsList;