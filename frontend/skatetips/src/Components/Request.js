import React from 'react'
import "./Request.css"
import { Link } from 'react-router-dom'

function Request(props) {
    let requestType = props.request.type.toLowerCase()
    requestType = requestType == "question" ? "request" : requestType;
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let user = bearer.id;

    console.log(props.request)
    let theirProfile = props.request.senderId == user ? props.request.receiverId : props.request.senderId;
    let otherUser = props.request.senderId == user
    let clss = otherUser != true  ? "single-request received" : "single-request sent"
    const [theirName, setTheirName] = React.useState("")
    React.useEffect(() => {
        switch (props.request.status) {
            case "FULLFILLED": {
                document.getElementById("single-request-" + props.request.id).style.borderColor = "green";
                document.getElementById("request-" + props.request.id + "-status").innerHTML = "ACCEPTED: "
                document.getElementById("request-" + props.request.id + "-status").style.color = "green";
                document.getElementById("request-" + props.request.id + "-status").style.fontWeight = "900";
                break;
            }
            case "REFUSED": {
                document.getElementById("single-request-" + props.request.id).style.borderColor = "red";
                document.getElementById("request-" + props.request.id + "-status").innerHTML = "REFUSED: "
                document.getElementById("request-" + props.request.id + "-status").style.color = "red";
                document.getElementById("request-" + props.request.id + "-status").style.fontWeight = "900";
                break;
            }
        }
        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("/users/" + theirProfile + "/name", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(theirName => setTheirName(theirName))
            .catch(err => console.log("error"))

        if (otherUser == true) {
            document.getElementById("delete-request-" + props.request.id).addEventListener('click', (e) => {
                document.getElementById("delete-request-" + props.request.id).disabled = true;

                let options = {
                    method: "DELETE",
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": localStorage.getItem("token")
                    },
                    body: JSON.stringify({
                        "requestId": props.request.id,
                        "senderId": props.request.senderId,
                        "receiverId": props.request.receiverId,
                        "status": props.request.status
                    })
                }
                fetch("/requests/my-requests/delete", options)
                    .then(response => {
                        if (response.status == 200) {
                            response = response.json();
                            let exRequest = document.getElementById("single-request-" + props.request.id)
                            //exRequest.style.position = "fixed"
                            exRequest.style.opacity = "0"
                            exRequest.style.marginRight = "-50vh";
                            let oldCount = parseInt(document.getElementById("sent-requests-header-span-count").innerHTML);
                            oldCount--;
                            document.getElementById("sent-requests-header-span-count").innerHTML = oldCount;
                            setTimeout(function () {

                                exRequest.parentNode.removeChild(exRequest)

                            }, 1000)

                        }
                        else if(response.status == 400){
                            alert("This request might have been deleted. Refreshing the page...")
                            window.location.reload()
                        }

                    })

            }
            )
        }

        else {
            document.getElementById("reject-request-" + props.request.id).addEventListener('click', (e) => {

                document.getElementById("reject-request-" + props.request.id).disabled = true;
                document.getElementById("accept-request-" + props.request.id).disabled = true;
                let options = {
                    method: "PUT",
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": localStorage.getItem("token")
                    },
                    body: JSON.stringify({
                        "requestId": props.request.id,
                        "senderId": props.request.senderId,
                        "receiverId": props.request.receiverId,
                        "status": "REFUSED"
                    })
                }

                fetch("/requests/my-requests/reply", options)
                    .then(response => {
                        if (response.status == 200) {
                            document.getElementById("single-request-" + props.request.id).style.borderColor = "red";
                            document.getElementById("request-" + props.request.id + "-status").innerHTML = "REFUSED: "
                            document.getElementById("request-" + props.request.id + "-status").style.color = "red";
                            document.getElementById("request-" + props.request.id + "-status").style.fontWeight = "900";
                            response = response.json();
                            let exRequest = document.getElementById("single-request-" + props.request.id)
                            exRequest.style.opacity = "0"
                            exRequest.style.marginLeft = "-50vh";
                            document.getElementById("request-" + props.request.id + "-status").innerHTML = "REFUSED: "
                            document.getElementById("request-" + props.request.id + "-status").color = "red";
                            let oldCount = parseInt(document.getElementById("received-requests-header-span-count").innerHTML);
                            oldCount--;
                            document.getElementById("received-requests-header-span-count").innerHTML = oldCount;
                            setTimeout(function () {
                                exRequest.parentNode.removeChild(exRequest)

                            }, 1000)
                        }
                        else if(response.status == 400){
                            alert("This request might've been deleted. Refreshing the page...")
                            window.location.reload();
                        }
                    })
            })

            document.getElementById("accept-request-" + props.request.id).addEventListener('click', (e) => {
                document.getElementById("accept-request-" + props.request.id).disabled = true;

                let options = {
                    method: "PUT",
                    headers: {
                        "Content-type": "application/json",
                        "Authorization": localStorage.getItem("token")
                    },
                    body: JSON.stringify({
                        "requestId": props.request.id,
                        "senderId": props.request.senderId,
                        "receiverId": props.request.receiverId,
                        "status": "FULLFILLED"
                    })
                }

                fetch("/requests/my-requests/reply", options)
                    .then(response => {
                        if (response.status == 200) {
                            response = response.json();
                            document.getElementById("single-request-" + props.request.id).style.borderColor = "green";
                            document.getElementById("request-" + props.request.id + "-status").innerHTML = "ACCEPTED: "
                            document.getElementById("request-" + props.request.id + "-status").style.color = "green";
                            document.getElementById("request-" + props.request.id + "-status").style.fontWeight = "900";
                        }
                        else if(response.status == 400){
                            alert("This request might've been deleted. Refreshing the page...")
                            window.location.reload();
                        }
                    })



            })
        }

    }, [])
    return (
        <div id={"single-request-" + props.request.id} className={clss}>
            <Link to={"/users/" + theirProfile} className="link-no-decoration">
                {otherUser == true ?
                    <span><span id={"request-" + props.request.id + "-status"}>{props.request.status}: </span>You <b>sent</b> a help <b>{requestType}</b> to {theirName.username}</span>
                    :
                    <span><span id={"request-" + props.request.id + "-status"}>{props.request.status}: </span>You <b>received</b> a help <b>{requestType}</b> from {theirName.username}</span>
                }
            </Link>
            <hr />
            <span className="single-request-description">
                {props.request.description}
            </span>
            <br />
            <br/>
            <div className="single-request-buttons-div">
                {otherUser == true && <button className="delete-request-button" id={"delete-request-" + props.request.id}><b>Delete request</b></button>}
                {otherUser != true &&
                    <div>
                        <button className="reject-request-button" id={"reject-request-" + props.request.id}><b>Refuse</b></button>
                        {props.request.status == "FULLFILLED" ? <button className="accept-request-button" id={"accept-request-" + props.request.id} disabled><b>Accept</b></button>
                            :
                            <button className="accept-request-button" id={"accept-request-" + props.request.id}><b>Accept</b></button>}

                    </div>}
            </div>
        </div>
    );

}

export default Request;