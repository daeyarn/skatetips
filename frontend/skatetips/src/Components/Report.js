import React from 'react'
import "./Report.css"
import {Link} from 'react-router-dom'

function Report(props){
    let {id, reporterId, reportedId, description} = props.report;

    let reportedName = "";

    React.useEffect(() => {

        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch("/users/"+reportedId+"/name", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(nameDTO => {
                document.getElementById("reported-name-span-"+id).innerHTML = nameDTO.username;
            })
        
            document.getElementById("delete-report-"+id+"-button").addEventListener('click', function(){
                document.getElementById("delete-report-"+id+"-button").disabled = true;
                let options = {
                    method: "DELETE",
                    headers:{
                        "Content-type": "application/json",
                        "Authorization": localStorage.getItem("token")
                    }
                    /*headers: JWT!*/
                }
                fetch("/reports/delete/"+id, options)
                    .then(response => {
                        console.log(response)
                        if(response.status == 200){
                            let exReport = document.getElementById("report-"+id)
                            exReport.style.opacity = "0";
                            exReport.style.marginTop = "-10vw";
                            setTimeout(e => 
                            exReport.parentNode.removeChild(exReport) , 1000)

                            let numberOfReports = document.getElementById("reports-fixed-header").innerHTML
                            numberOfReports = parseInt(numberOfReports.substr(numberOfReports.indexOf("(")+1, numberOfReports.indexOf(")")).replace(")", ""))
                            console.log(numberOfReports)
                            numberOfReports--;
                            document.getElementById("reports-fixed-header").innerHTML = "Reports("+numberOfReports+")"
                            
                        }
                        else window.location.reload();
                    })
            })

    }, [])

    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let authority = bearer.role;

    return(
        <div id = {"report-" + id} className = "single-report-div">
            <Link to = {"/users/"+reportedId} className = "link-no-decoration">
                <span>{authority == "ADMIN" ? "Someone" : "You"} reported <h3 id = "reported-name-h3"><span id = {"reported-name-span-"+id}>{reportedName}</span></h3> for:</span>
            </Link>
                <hr/>
               
                <span>{description}</span>
                <br /><br />
                
                <button id = {"delete-report-"+id+"-button"} className = "delete-report-button" type="submit">Delete</button>
            
        </div>


    );

    
}

export default Report;