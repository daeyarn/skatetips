import React from 'react'
import Video from "./Video.js"
import { Link } from 'react-router-dom'
import "./VideoList.css"
import { Tooltip } from '@varld/popover';

function VideoList(props) {
    let videoType = props.videos;
    const [videos, setVideos] = React.useState([])

    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let myId = bearer.id;
    let ownProfile = props.profileId == myId;
    React.useEffect(() => {

        if (ownProfile)
            document.getElementById("add-" + props.videos + "-video").addEventListener("click", function () {
                console.log("DA")
                switch (props.videos) {
                    case "tutorials": {
                        if (document.getElementById("video-input-div").style.marginTop == "0vh") {
                            document.getElementById("video-input-div").style.marginTop = "100vh";
                            document.getElementById("add-video-header-section").innerHTML = "";
                            return
                        }
                        document.getElementById("add-video-header-section").innerHTML = "tutorials";
                        document.getElementById("video-input-div").style.marginTop = "0vh"; break;
                    }
                    case "trials": {
                        if (document.getElementById("video-input-div").style.marginTop == "31.66vh") {
                            document.getElementById("video-input-div").style.marginTop = "100vh";
                            document.getElementById("add-video-header-section").innerHTML = "";
                            return
                        }
                        document.getElementById("add-video-header-section").innerHTML = "trials";
                        document.getElementById("video-input-div").style.marginTop = "31.66vh"; break;
                    }
                    case "steezy": {
                        if (document.getElementById("video-input-div").style.marginTop == "63.32vh") {
                            document.getElementById("video-input-div").style.marginTop = "100vh";
                            document.getElementById("add-video-header-section").innerHTML = "";
                            return
                        }
                        document.getElementById("add-video-header-section").innerHTML = "steezy";
                        document.getElementById("video-input-div").style.marginTop = "63.32vh"; break;
                    }
                }
            })

        let url = "/users/" + props.profileId + "/" + videoType;
        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        fetch(url, options).then(response => {
            if (response.status == 403) {
                alert("Session has expired. Please log in again to continue.")
                document.getElementById("logout").click()
                return;
            }
            return response.json()
        })
            .then(videos => {
                //console.log(videos)

                setVideos(videos)
                document.getElementById(props.videos + "-number").innerHTML = videos.length
            })
            .catch(err => console.log(err))
    }, [])

    return (
        <div className="video-list-div">
            {videos.map(video => <Video video={video} />)}
            {/*ownProfile && <Tooltip display = "inline" content = {"Add video to "+props.videos+"."}><div id = {"add-"+props.videos+"-video"} className = "add-video-div"></div></Tooltip>*/}
        </div>
    );
}

export default VideoList;