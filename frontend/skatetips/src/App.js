import logo from './logo.svg';
import './App.css';
import InitialBranch from './Pages/InitialBranch'
import React, {useEffect, useState} from 'react'
import Interaction from "./Pages/Interaction.js"
import Login from './Pages/Login.js'
import Registration from './Pages/Registration.js'
import Profile from "./Pages/Profile"
import Requests from "./Pages/Requests.js"
import Followings from "./Pages/Followings.js"
import Reports from "./Pages/Reports.js"
import AddTrick from "./Pages/AddTrick.js"
import MyProfile from "./Pages/MyProfile.js"
import SideBar from "./Components/Navigation/SideBar.js"
import {BrowserRouter, Switch, Route, Redirect} from 'react-router-dom'


//import LoginForm from "./Components/Form/LoginForm"
function App() {
	return(
		<BrowserRouter>
			<Switch>
				<Route path = "/login" exact component={Login}></Route>
				<Route path = "/register" exact component={Registration}></Route>
				<Route path = "/" exact component = {InitialBranch}></Route> 
				<Route path = "/interact" exact component = {Interaction} />
				<Route path = "/my-profile" exact component = {MyProfile}></Route>
				<Route path = "/users/:id" exact component = {Profile}></Route>
				<Route path = "/requests" exact component = {Requests}></Route>
				<Route path = "/followings" exact component = {Followings}></Route>
				<Route path = "/reports" exact component = {Reports}></Route>
				<Route path = "/logout">
					<Redirect to="/"></Redirect>
				</Route>
				<Route path = "/add-trick" exact component = {AddTrick}></Route>
			</Switch>
		</BrowserRouter>
	
		
	);
  
}

export default App;
