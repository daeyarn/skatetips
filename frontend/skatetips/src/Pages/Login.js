import { Formik, Form, Field } from 'formik'
import React from 'react'
import "./Login.css"

//login page
function Login(props) {

    const validate = (data) => {
        let label = document.getElementById("label");
        label.textContent = "";
        if (data.username == '' && data.password == '') {
            alert("Please enter login information.");
            return;
        }

        if (data.username == '') {
            alert("Please enter your username.")
            return;
        }

        if (data.password == '') {
            alert("Please enter your password.");
            return;
        }

        let options = {
            method: "POST",
            headers: {
                "Content-type": "application/json"
            },
            body: JSON.stringify({
                "username": data.username,
                "password": data.password
            }),
        }

        fetch("/login", options).then(
            response => {
                if (response.status !== 200) {
                    alert("Incorrect username or password.");
                    return;
                }
                else {
                    let bearer = response.headers.get("Authorization");
                    localStorage.setItem("token", bearer)
                    bearer = bearer.substr(6, bearer.length - 6)
                    var base64Payload = bearer.split('.')[1];
                    var payload = Buffer.from(base64Payload, 'base64');
                    bearer = JSON.parse(payload.toString());


                    props.history.push(`/interact`);
                }
            }

        ).catch(error => console.log(error));
        return null;
    };
    return (
        <div className="login-main-div">
            <div className="login-div">

                <Formik className="login-form-div"
                    initialValues={{ username: '', password: '' }}
                    onSubmit={(data, actions) =>
                        validate(data)
                    }

                >
                    <Form id="login-form">
                        <h1 id="login-h">Login</h1>
                        <hr />
                        <table>
                            <tr>
                                <td>
                                    username
                                </td>
                                <td>
                                    <Field className="login-input" as="input" name="username" placeholder="username"></Field>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    password
                                </td>
                                <td>
                                    <Field className="login-input" type="password" name="password" placeholder="password"></Field>
                                </td>


                            </tr>
                        </table>
                        <br />
                        <label id="label"></label>
                        <button id="login-button" type="submit">Login</button>
                    </Form>


                </Formik>

            </div>
        </div>
    );
}


export default Login;