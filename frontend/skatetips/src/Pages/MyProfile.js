import React from 'react'
import SideBar from "../Components/Navigation/SideBar.js"
import VideoSegment from "../Components/Profile/VideoSegment.js"
import ProfileInformation from "../Components/Profile/ProfileInformation.js"
import AddVideo from "../Components/Form/AddVideo.js"
import "./MyProfile.css"


//user's own profile
function MyProfile(props) {
    if (localStorage.getItem("token") == null) {
        window.location.href = "/";
    }
    React.useEffect(() => {
        document.getElementById("video-input-div")

        /*
                let mainDiv = document.getElementById("main-div-div")
                if(mainDiv != null) mainDiv.addEventListener("click", e=>{
                    let vidAddForm = document.getElementById("video-input-div")
                    if(vidAddForm) document.getElementById("video-input-div").style.marginTop = "100vh"
                })
            */

    }, [])
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let profileId = bearer.id;
    let selection = "name"
    return (
        <div className="profile-main-div">
            <SideBar selected={selection} />
            <div id="main-div-div" className="main-div-div">
                <VideoSegment id={"video-segment-" + profileId} />
                <ProfileInformation id={profileId} />
            </div>
            <AddVideo />
        </div>
    );

}

export default MyProfile;