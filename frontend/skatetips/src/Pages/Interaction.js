import React from 'react';
import InteractionForm from '../Components/Form/InteractionForm.js';
import RandomSkater from '../Components/Interaction/RandomSkater.js';
import SideBar from "../Components/Navigation/SideBar.js"
import "./Interaction.css"


//interaction page
function Interaction(props){
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    return(
        <div className = "main-div-interaction">
            <SideBar selected = "interact" />
            <div className = "main-div-div">
                <InteractionForm />
            </div>
            
        </div>
    );
}

export default Interaction;