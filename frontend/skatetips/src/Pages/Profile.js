import React, { useEffect } from 'react'
import SideBar from "../Components/Navigation/SideBar.js"
import VideoSegment from "../Components/Profile/VideoSegment.js"
import ProfileInformation from "../Components/Profile/ProfileInformation.js"
import "./Profile.css"
import { Formik, Form, Field } from 'formik'
import { Tooltip } from '@varld/popover'


//other user's profile
function Profile(props) {
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    let profileId = window.location.href.substr(window.location.href.lastIndexOf("/") + 1);
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let myId = bearer.id;
    if (profileId == myId) props.history.push("/my-profile")
    let selection = "interact"

    let requestOfferType = null;


    useEffect(() => {
        if(document.getElementById("request-form-offer-help-selection") == null) return
        document.getElementById("request-form-offer-help-selection").addEventListener('click', function(){
            if(requestOfferType == "OFFER"){
                document.getElementById("request-form-offer-help-selection").style.borderColor = "white"
                requestOfferType = null

            }

            else if(requestOfferType == "QUESTION"){
                document.getElementById("request-form-offer-help-selection").style.borderColor = "blue"
                document.getElementById("request-form-request-help-selection").style.borderColor = "white"
                requestOfferType = "OFFER"

            }

            else if(requestOfferType == null){
                document.getElementById("request-form-offer-help-selection").style.borderColor = "blue"
                requestOfferType = "OFFER"
            }

        })

        document.getElementById("request-form-request-help-selection").addEventListener('click', function(){
            if(requestOfferType == "QUESTION"){
                document.getElementById("request-form-request-help-selection").style.borderColor = "white"
                requestOfferType = null

            }

            else if(requestOfferType == "OFFER"){
                document.getElementById("request-form-offer-help-selection").style.borderColor = "white"
                document.getElementById("request-form-request-help-selection").style.borderColor = "blue"
                requestOfferType = "QUESTION"

            }

            else if(requestOfferType == null){
                document.getElementById("request-form-request-help-selection").style.borderColor = "blue"
                requestOfferType = "QUESTION"
            }

        })
    }, []
    
    )
    return (
        <div className="profile-main-div">
            <SideBar selected={selection} />
            <div className="main-div-div">
                <VideoSegment id={profileId} />
                <ProfileInformation id={profileId} />
            </div>
            <div id="request-popup-div" className="popup-div">
                <h2 id="make-request-header">Make request!</h2>
                <br />
                <Formik initialValues={{ "description": '' }}
                    onSubmit={(data, values) => {
                        if(data.description == '' || requestOfferType == null){
                            alert("You cannot send a blank request.")
                            return;
                        }

                        let options = {
                            method: "POST",
                            headers:{
                                "Content-type": "application/json",
                                "Authorization": localStorage.getItem("token")
                            },
                            body: JSON.stringify({
                                "senderId": myId,
                                "receiverId": profileId,
                                "requestType": requestOfferType,
                                "description": data.description
                            })
                        }

                        fetch("/requests/send", options)
                            .then(response => {
                                console.log(response)
                                if(response.status != 200){
                                    alert("Request sending failed. Check whether this user wants help.")
                                    return;
                                }
                                values.resetForm();
                                let offerType = requestOfferType == "OFFER" ? "offer" : "request";
                                document.getElementById("request-form-"+offerType+"-help-selection").style.borderColor = "white";
                                requestOfferType = null;
                                document.getElementById("request-button").click();
                            })

                    }


                    }>
                    <Form className="request-form">
                        <div className="request-form-offer-type-select">

                            <div id="request-form-offer-help-selection">

                            </div>
                            &nbsp;
                            <div id="request-form-request-help-selection">

                            </div>


                        </div>
                        <Field className="text-area-popup" as="textarea" name="description" placeholder="describe your request!"></Field>

                        <button id = "send-request-button" type="submit">Send</button>
                    </Form>
                </Formik>
            </div>
            <div id="report-popup-div">
                <h2 id="make-report-header">Make report</h2>
                <br />
                <Formik initialValues={{ "description": '' }}
                    onSubmit={(data, actions) => {
                        if (data.description == '') {
                            alert("You cannot send an empty report.")
                            return;
                        }
                        
                        let options = {
                            method: "POST",
                            headers: {
                                "Content-type": "application/json",
                                "Authorization": localStorage.getItem("token")
                            },
                            body: JSON.stringify({
                                "id": 0,
                                "reporterId": myId,
                                "reportedId": profileId,
                                "description": data.description
                            })
                        }

                        fetch("/users/" + profileId + "/report", options)

                            .then(response => {
                                console.log(response);
                                actions.resetForm();
                                document.getElementById("report-b").click();
                            })
                            .catch(error => console.log(error))

                        //props.history.push("/users/" + profileId)
                        //document.getElementById("report-popup-div").style.marginLeft = "140vw";
                    }

                    }>
                    <Form className="report-form">
                        <Field className="text-area-popup" as="textarea" name="description" placeholder="what is the issue with this profile?"></Field>
                        <button id = "report-button" type="submit">Report</button>
                    </Form>
                </Formik>
            </div>
        </div>
    );
}

export default Profile;