import React from 'react'
import SideBar from "../Components/Navigation/SideBar.js"
import FollowingsList from "../Components/FollowingsList.js"
import "./Followings.css"

//followings page
function Followings(props){
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let user = bearer.id;
    const [followings, setFollowings] = React.useState([])
    
    React.useEffect(() => {
        let options = {
            method: "POST",
            headers: {
                "Content-type": "application/json",
                "Authorization": localStorage.getItem("token")
            },
            
            body: JSON.stringify({
                skaterSenderId: user
            }), 
        }

        fetch("/users/"+user+"/followings", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(followings => setFollowings(followings))
            .catch(err => console.log("kakac"))

    }, [])
    return(
        <div className = "followings-main-div">
            <SideBar selected = "followings"/>
            <div className = "main-div-div">
                <div className = "main-div">
                    <div className = "followings-followers-div">
                        <div className = "following-header-div">
                            <span id = "followers-header-span">Followers(0)</span>
                        </div>
                        <br/>
                        {followings.length != 0 && <FollowingsList followings = {followings} className = "followings-followers" flag = "followers"/>}
                    </div>
                    <div className = "followings-followed-div">
                        <div className = "following-header-div">
                            <span id = "following-header-span">Following(0)</span>
                        </div>
                        <br/>
                        {followings.length != 0 && <FollowingsList followings = {followings} className = "followings-followed" flag = "followings"/>}
                    </div>
                    
                </div>
            </div>
        </div>
    );


}

export default Followings;