import React from 'react'
import { Formik, Form, Field } from 'formik'
import "./Registration.css"
import TrickList from "../Components/TrickList.js"
import Trick from "../Components/Trick.js"
import { findAllInRenderedTree } from 'react-dom/test-utils'


//registration form
function Registration(props) {
    const [tricks, setTricks] = React.useState([])
    localStorage.setItem("knownTricks", "")
    localStorage.setItem("learningTricks", "")

    React.useEffect(() => {
        fetch("tricks")
            .then(response => response.json())
            .then(tricks => setTricks(tricks))
            .catch(error => console.log(error))
    }, []);


    return (
        <div className="registration-main-div">
            <div className="registration-center-div">

                <Formik className="registration-form-div"
                    initialValues={{
                        username: '',
                        stance: "",
                        description: '',
                        wantsHelp: "",
                        pass: '',
                        repeatPass: '',
                        profilePic: ""
                    }}
                    onSubmit={(data, actions) => {
                        //console.log("registered")
                        if (data.username == '' || data.pass == '' || data.repeatPass == '' || data.stance == "" || data.wantsHelp == "") {
                            alert("Please fill in all required fields.")
                            //document.getElementById("registration-failed-warning").style.color = "white";
                            return;
                        }

                        if(data.description.length > 100){
                            alert("Description too long. Please make it 100 characters max.")
                            return;
                        }

                        if(data.username.length > 20){
                            alert("Username too long. Please make it 20 characters max.")
                            return;
                        }

                        if (data.pass != data.repeatPass) {
                            alert("Passwords do not match.")
                            //document.getElementById("registration-failed-warning").style.color = "white";
                            return;
                        }

                        if (data.pass.length < 8) {
                            alert("Please make your password at least 8 characters long.");
                            //document.getElementById("registration-failed-warning").style.color = "white";
                            return;
                        }

                        if(data.pass.length > 255){
                            alert("Password too long. Please make it 255 characters max.")
                            return
                        }


                        let knownTricks = localStorage.getItem("knownTricks");
                        let learningTricks = localStorage.getItem("learningTricks");

                        if (knownTricks == "") knownTricks = [];
                        else {
                            knownTricks = knownTricks.split("known")
                            knownTricks.pop()
                            let knownTricksTmp = []
                            for (let i = 0; i < knownTricks.length; i++) {
                                knownTricksTmp.push(parseInt(knownTricks[i]))
                            }

                            knownTricks = knownTricksTmp;
                        }



                        if (learningTricks == "") learningTricks = []
                        else {
                            learningTricks = learningTricks.split("learn")
                            learningTricks.pop()
                            let learningTricksTmp = []
                            for (let i = 0; i < learningTricks.length; i++) {
                                learningTricksTmp.push(parseInt(learningTricks[i]))
                            }

                            learningTricks = learningTricksTmp;
                        }

                        console.log(data.username)
                        console.log(data.stance)
                        console.log(data.wantsHelp)
                        /*console.log(learningTricks)
                        console.log(knownTricks)*/
                        let options = {
                            method: "POST",
                            headers: {
                                "Content-type": "application/json"
                            },
                            body: JSON.stringify({
                                "username": data.username,
                                "stance": data.stance,
                                "description": data.description,
                                "knownTricks": knownTricks,
                                "learningTricks": learningTricks,
                                "wantsHelp": data.wantsHelp,
                                "password": data.pass,
                                "profilePicture": data.profilePic
                            }),

                        }

                        fetch("/register", options)
                            .then(response => {

                                if (response.status == 200) {
                                    localStorage.removeItem("knownTricks");
                                    localStorage.removeItem("learningTricks");
                                    props.history.push("/login")
                                }
                                else if (response.status != 200) {
                                    document.getElementById("registration-failed-warning").innerHTML = "User with given username already exists. Please try with a new one.";
                                    document.getElementById("registration-failed-warning").style.color = "white";
                                }

                            })
                            .catch(err => console.log("err"))

                    }
                    }>
                    <Form className="registration-form">

                        <div id="registration-bordered-div">
                            <h1 className="registration-h1">Registration</h1>
                            <hr />
                            <table className = "info-input-form-table">
                                <tr>
                                    <td>
                                        <span maxlength = "20" className="white-text">username </span>
                                    </td>

                                    <td>
                                        <Field className="registration-input" as="input" name="username" placeholder="username" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="white-text">your stance </span>
                                    </td>
                                    <td>
                                        <Field className="registration-input" as="select" name="stance">
                                            <option id="no-display-option" value="stance">stance</option>
                                            <option name="goofy" value="GOOFY">goofy</option>
                                            <option name="regular" value="REGULAR">regular</option>
                                        </Field>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="white-text">do you want others to offer help </span>
                                    </td>
                                    <td>
                                        <Field className="registration-input" as="select" name="wantsHelp">
                                            <option id="no-display-option" value="stance">help</option>
                                            <option value={true}>yes</option>
                                            <option value={false}>no</option>
                                        </Field>
                                    </td>
                                </tr>
                                
                                <tr>
                                    <td>
                                        <span className="white-text">describe yourself </span>
                                    </td>
                                    <td>
                                        <Field maxlength = "100" className="registration-input" as="textarea" name="description" placeholder="description" rows="1" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="white-text">password </span>
                                    </td>

                                    <td>
                                        <Field maxlength = "255" className="registration-input" as="input" type="password" name="pass" placeholder="password" />
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <span className="white-text">repeat password </span>
                                    </td>
                                    <td>
                                        <Field className="registration-input" as="input" type="password" name="repeatPass" placeholder="repeat password" />

                                    </td>
                                </tr>
                            </table>
                            <div className="tricks">
                                <div>
                                    <div className="registration-header-div">
                                        <h1 className="registration-h1" id="tyk">Known tricks</h1>
                                    </div>
                                    <div className="registration-header-div">
                                        <h1 className="registration-h1" id="tyw">Learning tricks</h1>
                                    </div>
                                </div>
                                <div className="known-tricks">
                                    <div className="all-tricks">
                                        {tricks.map(trick =>

                                            <div id={trick.id + "known"} className="trick-with-button">
                                                <Trick learning={false} divId={trick.id + "known"} trick={trick} />
                                                {/*console.log(trick.id + "known")*/}
                                            </div>
                                        )}


                                    </div>
                                    <div className="chosen-tricks">

                                    </div>
                                </div>

                                <div className="known-tricks">
                                    <div className="all-tricks-right">
                                        {tricks.map(trick =>
                                            <div id={trick.id + "learn"} className="trick-with-button-right">
                                                <Trick learning={true} divId={trick.id + "learn"} trick={trick} />

                                            </div>
                                        )}
                                    </div>
                                    <div className="chosen-tricks">

                                    </div>
                                </div>
                            </div>
                            <label id="registration-failed-warning"></label>
                            <br />
                            <br />
                            <button id="register-button" type="submit">Register</button>
                        </div>
                    </Form>


                </Formik>


            </div>
        </div>
    );
}

export default Registration;