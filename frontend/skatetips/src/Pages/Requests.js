import React from 'react'
import SideBar from "../Components/Navigation/SideBar"
import "./Requests.css"
import {useState, useEffect} from 'react'
import RequestList from "../Components/RequestList.js"


//requests page
function Requests(props){
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    const [requests, setRequests] = React.useState([])
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let user = bearer.id;
   
    let receivedShowAnsweredPressed = false
    let receivedNotAnsweredPressed = false

    let sentShowAnsweredPressed = false
    let sentNotAnsweredPressed = false
    
   useEffect(()=>{
        
        let options = {
            method: "POST",
            headers: {
                "Content-type": "application/json", //add jwt!
                "Authorization": localStorage.getItem("token")
            },
            body: JSON.stringify({
                skaterId: user
            }),
        }
        fetch("/requests/my-requests", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return;
                }
                return response.json()
            })
            .then(requests => setRequests(requests))
            .catch(error => console.log("error"))

            /*
            setTimeout(e => {
                fetch("/requests/my-requests", options)
            .then(response => response.json())
            .then(requests => setRequests(requests))
            .catch(error => console.log("error"))
            }, 30000)
            */
        
            document.getElementById("show-only-answered-received-filter").addEventListener('click', e => {
                document.getElementById("show-all-answered-received-filter").style.borderColor = "white"
                document.getElementById("show-not-answered-received-filter").style.borderColor = "white"
                document.getElementById("show-only-answered-received-filter").style.borderColor = "blue"
                let requests = document.getElementsByClassName("received")
                if(receivedShowAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    receivedShowAnsweredPressed = false;
                    return;
                }
                
                if(receivedNotAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    receivedNotAnsweredPressed = false;
                }

                for(let i=0; i<requests.length; i++){
                    if(requests[i].style.display == "none") continue
                    let status = document.getElementById(requests[i].id.replace("single-", "")+"-status").innerHTML.replace(" ", "")
                    if(status.replace(" ", "") == "PENDING:") (requests[i]).style.display = "none"
                }
                receivedShowAnsweredPressed = true;
            })

            document.getElementById("show-not-answered-received-filter").addEventListener('click', e => {
                document.getElementById("show-all-answered-received-filter").style.borderColor = "white"
                document.getElementById("show-only-answered-received-filter").style.borderColor = "white"
                document.getElementById("show-not-answered-received-filter").style.borderColor = "blue"
                let requests = document.getElementsByClassName("received")
                if(receivedNotAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    receivedNotAnsweredPressed = false;
                    return;
                }

                if(receivedShowAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    receivedShowAnsweredPressed = false;
                }
                
                for(let i=0; i<requests.length; i++){
                    if(requests[i].style.display == "none") continue
                    let status = document.getElementById(requests[i].id.replace("single-", "")+"-status").innerHTML.replace(" ", "")
                    if(status.replace(" ", "") != "PENDING:") (requests[i]).style.display = "none"
                }
                receivedNotAnsweredPressed = true;
            })

            document.getElementById("show-all-answered-received-filter").addEventListener("click", e => {
                document.getElementById("show-only-answered-received-filter").style.borderColor = "white"
                document.getElementById("show-not-answered-received-filter").style.borderColor = "white"
                document.getElementById("show-all-answered-received-filter").style.borderColor = "blue"
                receivedNotAnsweredPressed = false;
                receivedShowAnsweredPressed = false;
                let requests = document.getElementsByClassName("received")
                for(let i=0; i<requests.length; i++){
                    requests[i].style.display = "block";
                }
            })

            //for sent requests
            document.getElementById("show-only-answered-sent-filter").addEventListener('click', e => {
                document.getElementById("show-all-answered-sent-filter").style.borderColor = "white"
                document.getElementById("show-not-answered-sent-filter").style.borderColor = "white"
                document.getElementById("show-only-answered-sent-filter").style.borderColor = "blue"
                let requests = document.getElementsByClassName("sent")
                if(sentShowAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    sentShowAnsweredPressed = false;
                    return;
                }
                
                if(sentNotAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    sentNotAnsweredPressed = false;
                }

                for(let i=0; i<requests.length; i++){
                    if(requests[i].style.display == "none") continue
                    let status = document.getElementById(requests[i].id.replace("single-", "")+"-status").innerHTML.replace(" ", "")
                    if(status.replace(" ", "") == "PENDING:") (requests[i]).style.display = "none"
                }
                sentShowAnsweredPressed = true;
            })

            document.getElementById("show-not-answered-sent-filter").addEventListener('click', e => {
                document.getElementById("show-all-answered-sent-filter").style.borderColor = "white"
                document.getElementById("show-only-answered-sent-filter").style.borderColor = "white"
                document.getElementById("show-not-answered-sent-filter").style.borderColor = "blue"
                let requests = document.getElementsByClassName("sent")
                if(sentNotAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    sentNotAnsweredPressed = false;
                    return;
                }

                if(sentShowAnsweredPressed == true){
                    for(let i=0; i<requests.length; i++){
                        requests[i].style.display = "block";
                    }
                    sentShowAnsweredPressed = false;
                }
                
                for(let i=0; i<requests.length; i++){
                    if(requests[i].style.display == "none") continue
                    let status = document.getElementById(requests[i].id.replace("single-", "")+"-status").innerHTML.replace(" ", "")
                    if(status.replace(" ", "") != "PENDING:") (requests[i]).style.display = "none"
                }
                sentNotAnsweredPressed = true;
            })

            document.getElementById("show-all-answered-sent-filter").addEventListener("click", e => {
                document.getElementById("show-only-answered-sent-filter").style.borderColor = "white"
                document.getElementById("show-not-answered-sent-filter").style.borderColor = "white"
                document.getElementById("show-all-answered-sent-filter").style.borderColor = "blue"
                sentNotAnsweredPressed = false;
                sentShowAnsweredPressed = false;
                let requests = document.getElementsByClassName("sent")
                for(let i=0; i<requests.length; i++){
                    requests[i].style.display = "block";
                }
            })
            /*
        setInterval(function(){
            let options = {
                method: "POST",
                headers: {
                    "Content-type": "application/json", //add jwt!
                    "Authorization": localStorage.getItem("token")
                },
                body: JSON.stringify({
                    skaterId: user
                }),
            }

            fetch("/requests/my-requests", options)
            .then(response => response.json())
            .then(requests => setRequests(requests))
            .catch(error => console.log("error"))
        }, 30000) */
    },[])
        
        
    return(
        <div className = "requests-main-div">
            <SideBar selected = "requests"/>
            <div className = "main-div-div">
                <div className = "main-div">
                    <div className = "received-requests-div">
                        <div className = "request-header-div">
                            <span id = "received-requests-header-span" className = "request-header">Received requests(<span id = "received-requests-header-span-count">0</span>) | Show: </span>
                            <div className = "filter-requests-button" id = "show-all-answered-received-filter">all</div>
                            <div className = "filter-requests-button" id = "show-only-answered-received-filter">answered</div>
                            <div className = "filter-requests-button" id = "show-not-answered-received-filter">not answered</div>
                            
                        </div>
                        <br/>
                        
                            {requests.length != 0 && <RequestList flag = {"received"} requests = {requests} />}
                        
                    </div>
                    <div className = "sent-requests-div">
                        <div className = "request-header-div">
                            <span id = "sent-requests-header-span" className = "request-header">Sent requests(<span id = "sent-requests-header-span-count">0</span>) | Show: </span>
                            <div className = "filter-requests-button" id = "show-all-answered-sent-filter">all</div>
                            <div className = "filter-requests-button" id = "show-only-answered-sent-filter">answered</div>
                            <div className = "filter-requests-button" id = "show-not-answered-sent-filter">not answered</div>
                            
                        </div>
                        <br/>
                        
                            {requests.length != 0 && <RequestList flag = {"sent"} requests = {requests} />}
                        
                        
                    </div> 
                </div>

            </div>
        </div>
    );

}

export default Requests;