import React from 'react'
import SideBar from "../Components/Navigation/SideBar"
import AddTrickFormUser from "../Components/Form/AddTrickFormUser.js"
import AddTrickFormAdmin from "../Components/Form/AddTrickFormAdmin.js"


//form for adding a trick
function AddTrick(props) {
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let authority = bearer.role;
    if(authority != "ADMIN" && authority != "USER"){
        localStorage.removeItem("token");
        props.history.push("/login")
    }
    return (
        <div className="add-trick-main-div">
            <SideBar selected="add-trick" />
            <div className="main-div-div">
                <div className="main-div">
                    {authority == "ADMIN" ? <AddTrickFormAdmin /> : <AddTrickFormUser />}
                </div>

            </div>
        </div>
    );

}

export default AddTrick;