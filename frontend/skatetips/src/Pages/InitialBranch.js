import "./InitialBranch.css"
import LoginForm from "../Components/Form/LoginForm.js"
import RegistrationForm from "../Components/Form/RegistrationForm.js"
import SideBar from "../Components/Navigation/SideBar.js"
import { Redirect, Route, useHistory, Link} from 'react-router-dom';
import React from 'react'


//initial branch, gives choice for login or register
function InitialBranch(props){

    return(
        <div className = "main-div-initial">
            <Link to="/login" className = "link-no-decoration">
                <div className = "login-branch">
                </div>
            </Link> 

            <Link to="/register" className = "link-no-decoration">
                <div className = "registration-branch">
                </div>
            </Link>
        </div>

    );

}


export default InitialBranch;