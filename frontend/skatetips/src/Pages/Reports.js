import React from 'react'
import SideBar from "../Components/Navigation/SideBar.js"
import Report from "../Components/Report.js"
import "./Reports.css"


//reports page
function Reports(props){
    if(localStorage.getItem("token") == null){
		window.location.href = "/";
	}
    const [reports, setReports] = React.useState([]);
    let bearer = localStorage.getItem("token")
    bearer = bearer.substr(6, bearer.length - 6)
    var base64Payload = bearer.split('.')[1];
    var payload = Buffer.from(base64Payload, 'base64');
    bearer = JSON.parse(payload.toString());
    let authority = bearer.role;
    let myId = bearer.id;

    React.useEffect( () => {

        let options = {
            headers: {
                "Authorization": localStorage.getItem("token")
            }
        }
        if(authority == "ADMIN"){
            fetch("/reports", options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return
                }
                return response.json()
            })
            .then(reports => {
                setReports(reports)
                document.getElementById("reports-fixed-header").innerHTML = "Reports("+reports.length+")"
            })

            return;
        }

        fetch("/reports/"+myId, options)
            .then(response => {
                if(response.status == 403){
                    alert("Session has expired. Please log in again to continue.")
                    document.getElementById("logout").click()
                    return
                }
                return response.json()
            })
            .then(reports => {
                setReports(reports)
                document.getElementById("reports-fixed-header").innerHTML = "Reports("+reports.length+")"
            })

    }, [])
    return(
        <div className = "reports-main-div">
            <SideBar selected = "reports"/>
            <div className = "main-div-div">
                <div className = "main-div">
                    	<div id = "reports-fixed-header">Reports(0)</div>
                        <div className = "reports-list-div">
                            {reports.length != 0 && reports.map(report => <Report report = {report}/>)}
                        </div>
                </div>
            </div>

        </div>
    );
}

export default Reports;